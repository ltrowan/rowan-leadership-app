
require 'csv'

desc 'Data migration from csv'
namespace :template_data do  
  desc "Update tags for template"
  task :update_tags => :environment do
  	CSV.foreach("template_tags.csv", :headers => true) do |row|
  		template = MasterTemplate.find_by(template_type: row["TYPE"].strip)
  		template.tag_list = row["TAGS"].split(',').map(&:strip).join(',')
  		template.save
    end		
  end

  task :rename_templates => :environment do
    desc "rename 'Learning Principles Questionnaire A' and 'Learning Principles Questionnaire B' templates"

    [
        {old_name: 'Learning Principles Questionnaire A', new_name: "Assessing Current Learning Interventions", type: "ASSESSING_CURRENT_LEARNING_INTERVENTIONS"},
        {old_name: 'Learning Principles Questionnaire B', new_name: "Leverage Role Models", type: "LEVERAGE_ROLE_MODELS"}
    ].each do |template|
      master_template = MasterTemplate.find_by(name: template[:old_name])
      master_template.update(name: template[:new_name], template_type: template[:type])
    end

  end

end
