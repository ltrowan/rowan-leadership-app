module AdminData
  class SubmenuFooter < ActiveAdmin::Views::Footer
    def build
      super
      render('admin/submenu_data')
    end
   end
 end