## The migrated repo of the Rowan Leadership App

### Installation
* Clone repository
* Switch to ruby 2.1.2 (I used RVM)
* `bundle install`
* Install elasticsearch. Start instance of elasticsearch (see details below). You may need to reindex the user model by firing up a rails console and `User.reindex`. That should be the extent of it.
* Start webserver. 
* Win.

### Requirements:

* Needs puma to run in production, though I use webrick in dev. Handled at the Gemfile level, but you need to fire up the service if you're launching in production. I believe this just uses the `puma.conf` that's loaded with the gem (`/home/ubuntu/leadership/shared/bundle/ruby/2.1.0/gems/puma-3.9.1/tools/jungle/upstart/puma.conf`).

* Needs environment variables to be set. It uses `dotenv` to do this in both 
production and development to save on time. I put an entry in the LastPass folder for `.env (Leadership App)` to securely preserve these values.

* Needs local instance of elasticsearch to run. See third gotcha below for proper version. Configuration is minimal as `searchkick` will take care of almost all of it. Just needs to be listening on the default port (9200)

### Gotchas:

* Don't just `rails s` this thing unless you're configured properly. If you're setup to bind to port 3000 of `localhost`, on some OSes (my dev MacOS, or example), `localhost` will bind to the IPv6 address by default. This app cannot support an IPv6 address by design, and will fail after you log in because of the way it typecasts the address to an Integer. You'll get an *Integer out of range*-like error if you do. The only way to rectify this is to go into the rails console, find the errant user, and remove the IPv6 associated in the `last_ip_signed_in` (or similar) attribute. **To prevent this, start with an explicit binding to a IPv4 address:** `rails s webrick -b 0.0.0.0`

* The `Gemfile` has been peppered with explicit versioning, almost all of which are outdated. The code has idiosyncrasies that depend on the outdated versions of these gems. Change these versions only if you have the time and capacity to chase down odd and errant Angular and Rails-like behavior. Dragons be here. You have been warned.
  
* Further, these outdated gems require some outdated services on the backend. Namely, searchkick, and particularly the `search_all_users` method of the `User` controller, requires elasticsearch 1.7.5. I think they're on like version 5+ by now. If you're developing on a Mac, [this may be helpful](http://www.inanzzz.com/index.php/post/5td9/installing-a-specific-version-of-elasticsearch-in-mac-os). Otherwise, I used [this download page](https://www.elastic.co/downloads/past-releases/elasticsearch-1-7-5), along with [these installation instructions](https://www.digitalocean.com/community/tutorials/how-to-install-and-configure-elasticsearch-on-ubuntu-16-04), to get things up and running on an Ubuntu 16.04 server. Note that I had to symlink the config files from `/etc/elasticsearch` to `/usr/share/elasticsearch/config`, and modify a bunch of file permissions to get this to work. It was failing saying you had to configure log4js. Reading the trace closely, this was because it was looking at `/usr/share/elasticsearch/config` to house the `logging.yml`, not `/etc/elasticsearch` which is where the installer put them. Who knows.

This was pulled out of a Heroku deployment environment with a few hiccups.