country = Country.where(name:"United States")
if country.length==0
	country = Country.create(name: "United States")

	state = country.states.create(name: "California")
	state.base_locations.create(name: "Fort Irwin")

	state = country.states.create(name: "Georgia")
	state.base_locations.create(name: "Fort Benning")

	state = country.states.create(name: "Kansas")
	state.base_locations.create(name: "Fort Leavenworth")
	state.base_locations.create(name: "Fort Riley")	

	state = country.states.create(name: "Kentucky")
	state.base_locations.create(name: "Fort Knox")

	state = country.states.create(name: "Maryland")
	state.base_locations.create(name: "Fort Meade")
	state.base_locations.create(name: "Fort Detrick")	

	state = country.states.create(name: "New York")
	state.base_locations.create(name: "Fort Hamilton")
	state.base_locations.create(name: "West Point")	

	state = country.states.create(name: "North Carolina")
	state.base_locations.create(name: "Fort Bragg")
else
  country[1..country.length].each do |c|
  	c.destroy
  end	
end


  	['CDT','2LT','1LT','CPT','MAJ','LTC','COL','BG','MG','LTG','GEN'].each_with_index do |rank_name,index|
  		rank = Rank.where(name:rank_name)
  		if rank.length==0
  			Rank.create(name: rank_name,rowgroup_id:1,order:index+1)
  		else	
	  		rank[1..rank.length].each do |r|
	  			r.destroy
	  		end	
	  		rr = rank.first
	  		rr.rowgroup_id = 1
	  		rr.order = index+1
	  		rr.save
	  	end	
  	end	

  	['W01','CW2','CW3','CW4','CW5'].each_with_index do |rank_name,index|
  		rank = Rank.where(name:rank_name)
  		if rank.length==0
  			Rank.create(name: rank_name,rowgroup_id:1,order:index+1)
  		else	
	  		rank[1..rank.length].each do |r|
	  			r.destroy
	  		end	
	  		rr = rank.first
	  		rr.rowgroup_id = 2
	  		rr.order = index+1
	  		rr.save
	  	end	
  	end	

    ['PV1','PV2','PFC','SPC','SGT','SSG','SFC','MSG','1SG','SGM','CSM'].each_with_index do |rank_name,index|
  		rank = Rank.where(name:rank_name)
  		if rank.length==0
  			Rank.create(name: rank_name,rowgroup_id:1,order:index+1)
  		else	
	  		rank[1..rank.length].each do |r|
	  			r.destroy
	  		end	
	  		rr = rank.first
	  		rr.rowgroup_id = 3
	  		rr.order = index+1
	  		rr.save
	  	end	
  	end

#TODO no need to check for the presence of record with find_or_create_by method.
MasterTemplate.find_or_create_by(name: 'Be A Mentor', template_type: 'BE_A_MENTOR') 
MasterTemplate.find_or_create_by(name: 'Command Unit leader Development Scorecard', template_type: 'COMMAND_UNIT_LEADER_DEVELOPMENT_SCORECARD')  
MasterTemplate.find_or_create_by(name: 'Daily Leader Observations Planner', template_type: 'DAILY_LEADER_OBSERVATIONS_PLANNER') 
MasterTemplate.find_or_create_by(name: 'Assignment Demands Assessment', template_type: 'ASSIGNMENT_DEMANDS_ASSESSMENT') 
MasterTemplate.find_or_create_by(name: 'Getting To Know Tool', template_type: 'GETTING_TO_KNOW_TOOL') 
MasterTemplate.find_or_create_by(name: 'Individual Plans For Advancement', template_type: 'INDIVIDUAL_PLANS_FOR_ADVANCEMENT')
MasterTemplate.find_or_create_by(name: 'Leadership Selection Guidance', template_type: 'LEADERSHIP_SELECTION_GUIDANCE')
MasterTemplate.find_or_create_by(name: 'Assessing Current Learning Interventions', template_type: 'ASSESSING_CURRENT_LEARNING_INTERVENTIONS')
MasterTemplate.find_or_create_by(name: 'Leverage Role Models', template_type: 'LEVERAGE_ROLE_MODELS')
MasterTemplate.find_or_create_by(name: 'Training And Professional Development', template_type: 'TRAINING_AND_PROFESSIONAL_DEVELOPMENT')
MasterTemplate.find_or_create_by(name: 'Soar Observation Source', template_type: 'SOAR_OBSERVATION_SOURCE')
MasterTemplate.find_or_create_by(name: 'Study Tool', template_type: 'STUDY_TOOL')
MasterTemplate.find_or_create_by(name: 'Tiger Team Implementation Tool', template_type: 'TIGER_TEAM_IMPLEMENTATION_TOOL')
MasterTemplate.find_or_create_by(name: 'Unit Leader Development Roles And Responsibilities', template_type: 'UNIT_LEADER_DEVELOPMENT_ROLES_AND_RESPONSIBILITIES')

# Add some default master template to existing template content block
Template.where(master_template_id: nil).each do |template_content_block|
	template_content_block.master_template = MasterTemplate.first
	template_content_block.save
end	


