class CreateUserTemplates < ActiveRecord::Migration
  def change
    create_table :user_templates do |t|
      t.integer :user_id
      t.integer :master_template_id
      t.string :name
      t.text :template_data
      t.string :guid
      t.boolean :status

      t.timestamps null: false
    end
  end
end
