class AddVideoUrl < ActiveRecord::Migration
  def change
    add_column :video_verticals, :video_url, :string
    add_column :video_horizontals, :video_url, :string
  end
end
