class CreateQuickLinks < ActiveRecord::Migration
  def change
    create_table :quick_links do |t|
      t.integer :tab_id
      t.integer :position
      t.string :title
      t.string :url

      t.timestamps
    end
  end
end
