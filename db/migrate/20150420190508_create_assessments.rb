class CreateAssessments < ActiveRecord::Migration
  def change
    create_table :assessments do |t|
      t.string :title
      t.string :anchor_name
      t.string :sub_title
      t.string :time
      t.text :description

      t.timestamps
    end
  end
end
