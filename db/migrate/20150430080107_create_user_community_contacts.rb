class CreateUserCommunityContacts < ActiveRecord::Migration
  def change
    create_table :user_community_contacts do |t|
      t.integer :user_community_id
      t.integer :contact_id

      t.timestamps
    end
  end
end
