class AddParentPostIdToPost < ActiveRecord::Migration
  def change
    add_column :content_block_posts, :parent_post_id, :integer
  end
end
