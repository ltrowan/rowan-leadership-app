class CreateLeaderProfessionalDevelopments < ActiveRecord::Migration
  def change
    create_table :leader_professional_developments do |t|
      t.string :title
      t.string :subtitle

      t.timestamps null: false
    end
  end
end
