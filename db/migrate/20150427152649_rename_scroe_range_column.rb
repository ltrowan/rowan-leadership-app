class RenameScroeRangeColumn < ActiveRecord::Migration
  def change
    rename_column :score_ranges, :end, :ending
  end
end
