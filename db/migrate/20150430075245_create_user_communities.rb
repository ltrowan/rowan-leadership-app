class CreateUserCommunities < ActiveRecord::Migration
  def change
    create_table :user_communities do |t|
      t.string :name
      t.string :color
      t.boolean :status
      t.integer :user_id
      t.timestamps
    end
  end
end
