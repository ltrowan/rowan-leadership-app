class CreateMedia < ActiveRecord::Migration
  def change
    create_table :media do |t|
      t.attachment :file
      t.integer :assessment_id

      t.timestamps
    end
  end
end
