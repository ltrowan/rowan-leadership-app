class CreateImages < ActiveRecord::Migration
  def change
    create_table :images do |t|
      t.attachment :pc_image
      t.attachment :mobile_image
      t.integer :tab_id
      t.integer :height
      t.timestamps
    end
  end
end
