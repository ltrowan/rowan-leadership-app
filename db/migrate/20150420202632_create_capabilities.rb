class CreateCapabilities < ActiveRecord::Migration
  def change
    create_table :capabilities do |t|
      t.string :sub_title
      t.attachment :background_photo
      t.text :main_text
      t.string :type
      t.integer :score_range_id
      t.timestamps
    end
  end
end
