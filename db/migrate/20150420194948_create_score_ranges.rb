class CreateScoreRanges < ActiveRecord::Migration
  def change
    create_table :score_ranges do |t|
      t.integer :start
      t.integer :end
      t.integer :assessment_id
      t.timestamps
    end
  end
end
