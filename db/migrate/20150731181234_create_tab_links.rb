class CreateTabLinks < ActiveRecord::Migration
  def change
    create_table :tab_links do |t|
      t.string :title
      t.string :url
      t.integer :tab_id
      t.integer :position
      t.timestamps null: false
    end
  end
end
