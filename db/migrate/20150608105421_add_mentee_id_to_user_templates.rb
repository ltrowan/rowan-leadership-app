class AddMenteeIdToUserTemplates < ActiveRecord::Migration
  def change
    add_column :user_templates, :mentee_id, :integer
  end
end
