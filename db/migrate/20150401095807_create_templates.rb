class CreateTemplates < ActiveRecord::Migration
  def change
    create_table :templates do |t|
      t.attachment :image
      t.integer :width
      t.integer :alpha
      t.timestamps
    end
  end
end
