class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.string :title
      t.integer :stops_count
      t.integer :position
      t.string :intro_text
      t.text :question_text
      t.integer :assessment_id
      t.timestamps
    end
  end
end
