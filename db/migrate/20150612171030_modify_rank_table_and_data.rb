class ModifyRankTableAndData < ActiveRecord::Migration
  def change
  	add_column :ranks, :rowgroup_id, :int
  	add_column :ranks, :order, :int
  end
end
