class CreateSections < ActiveRecord::Migration
  def change
    create_table :sections do |t|
      t.string :title
      t.integer :position
      t.string :time
      t.integer :tab_id
      t.timestamps
    end
  end
end
