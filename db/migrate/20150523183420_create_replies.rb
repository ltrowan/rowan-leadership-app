class CreateReplies < ActiveRecord::Migration
  def change
    create_table :replies do |t|
      t.integer :content_block_post_id
      t.text :content
      t.integer :user_id
      t.timestamps
    end
  end
end
