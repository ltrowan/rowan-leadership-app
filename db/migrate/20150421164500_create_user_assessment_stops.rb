class CreateUserAssessmentStops < ActiveRecord::Migration
  def change
    create_table :user_assessment_stops do |t|
      t.string :stop_id
      t.text :response

      t.timestamps
    end
  end
end
