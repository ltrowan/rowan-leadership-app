class CreateVideoStatuses < ActiveRecord::Migration
  def change
    create_table :video_statuses do |t|
      t.integer :user_id
      t.string :video_type
      t.integer :content_block_post_id
      t.boolean :processed, default: false
      t.timestamps null: false
    end
  end
end
