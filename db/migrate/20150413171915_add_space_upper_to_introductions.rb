class AddSpaceUpperToIntroductions < ActiveRecord::Migration
  def change
    add_column :introductions, :space_upper, :string
  end
end
