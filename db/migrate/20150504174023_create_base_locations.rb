class CreateBaseLocations < ActiveRecord::Migration
  def change
    create_table :base_locations do |t|
      t.string :name
      t.string :state_id

      t.timestamps
    end
  end
end
