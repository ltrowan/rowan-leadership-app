class AddLeaderProfessionalDevelopmentIdToLpd < ActiveRecord::Migration
  def change
    add_column :lpds, :leader_professional_development_id, :integer
  end
end
