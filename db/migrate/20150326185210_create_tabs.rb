class CreateTabs < ActiveRecord::Migration
  def change
    create_table :tabs do |t|
      t.string :title
      t.string :anchor_name
      t.string :url
      t.string :time
      t.float :frame_size
      t.text :description

      t.timestamps
    end
  end
end
