class CreateLpds < ActiveRecord::Migration
  def change
    create_table :lpds do |t|
      t.string :design
      t.string :subject
      t.attachment :image
      t.string :subtitle
      t.string :time
      t.string :lpd_anchor_name
      t.string :description

      t.timestamps
    end
  end
end
