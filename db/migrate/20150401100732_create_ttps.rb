class CreateTtps < ActiveRecord::Migration
  def change
    create_table :ttps do |t|
      t.attachment :audio
      t.string :text

      t.timestamps
    end
  end
end
