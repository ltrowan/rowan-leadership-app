class CreateMasterTemplates < ActiveRecord::Migration
  def change
    create_table :master_templates do |t|
      t.string :name
      t.string :template_type
      t.text :description

      t.timestamps null: false
    end
  end
end
