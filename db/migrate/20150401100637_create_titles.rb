class CreateTitles < ActiveRecord::Migration
  def change
    create_table :titles do |t|
      t.string :text
      t.string :animation
      t.integer :width
      t.string :justification
      t.timestamps
    end
  end
end
