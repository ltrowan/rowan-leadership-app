class CreateIntroductions < ActiveRecord::Migration
  def change
    create_table :introductions do |t|
      t.integer :width
      t.integer :alpha
      t.string :bg_color
      t.integer :position
      t.string :text
      t.integer :body_width
      t.integer :body_alpha
      t.string :body_text

      t.timestamps
    end
  end
end
