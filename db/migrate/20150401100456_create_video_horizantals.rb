class CreateVideoHorizantals < ActiveRecord::Migration
  def change
    create_table :video_horizantals do |t|
      t.integer :width
      t.integer :alpha
      t.boolean :hide_text
      t.string :place_text
      t.string :text
      t.integer :video_width
      t.attachment :video

      t.timestamps
    end
  end
end
