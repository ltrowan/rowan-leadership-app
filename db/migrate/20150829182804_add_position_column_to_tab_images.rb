class AddPositionColumnToTabImages < ActiveRecord::Migration
  def change
  	add_column :images, :position, :integer, :default => 1
  end
end
