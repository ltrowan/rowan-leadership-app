class AddBackgroundImageToLpds < ActiveRecord::Migration
  def self.up
    change_table :leader_professional_developments do |t|
      t.attachment :background_image
    end
  end

  def self.down
    remove_attachment :leader_professional_developments, :background_image
  end
end
