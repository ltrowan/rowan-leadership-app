class IndexUserOnName < ActiveRecord::Migration
  def change
    change_table :users do |t|
      t.index :first_name
      t.index :last_name
      t.index [:first_name, :last_name]
    end
  end
end
