class CreatePostVideos < ActiveRecord::Migration
  def change
    create_table :post_videos do |t|
      t.string :title
      t.attachment :video

      t.timestamps
    end
  end
end
