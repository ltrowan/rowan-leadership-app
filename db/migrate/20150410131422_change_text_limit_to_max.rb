class ChangeTextLimitToMax < ActiveRecord::Migration
  def change
  	rename_table :video_horizantals, :video_horizontals
  	change_column :text_boxes, :text, :text
  	change_column :titles, :text, :text
  	change_column :video_verticals, :text, :text
  	change_column :video_horizontals, :text, :text
  	change_column :ttps, :text, :text
	change_column :introductions, :text, :text
	change_column :introductions, :body_text, :text
	change_column :lpds, :description, :text
	change_column :lpds, :subject, :text
  	add_column :templates,:text,:text
  end
end
