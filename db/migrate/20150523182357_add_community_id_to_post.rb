class AddCommunityIdToPost < ActiveRecord::Migration
  def change
    add_column :content_block_posts, :community_id, :integer
  end
end
