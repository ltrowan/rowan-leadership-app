class CreatePostImages < ActiveRecord::Migration
  def change
    create_table :post_images do |t|
      t.string :title
      t.attachment :image

      t.timestamps
    end
  end
end
