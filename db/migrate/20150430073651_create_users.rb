class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :first_name
      t.string :last_name
      t.integer :rank_id
      t.integer :state_id
      t.integer :country_id
      t.integer :base_location_id
      t.string :miltary_email
      t.attachment :image

      t.timestamps
    end
  end
end
