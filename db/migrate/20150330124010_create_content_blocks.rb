class CreateContentBlocks < ActiveRecord::Migration
  def change
    create_table :content_blocks do |t|
      t.string :title
      t.string :block_type
      t.integer :block_id
      t.string :anchor_name
      t.integer :position
      t.string :url
      t.string :quick_navigation_title
      t.integer :spacing
      t.integer :section_id
      #t.integer :width
      #t.integer :alpha
      #t.string :justification
      
      
      t.timestamps
    end
  end
end
