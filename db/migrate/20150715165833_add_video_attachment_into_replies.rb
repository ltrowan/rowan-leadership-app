class AddVideoAttachmentIntoReplies < ActiveRecord::Migration
  def self.up
    change_table :replies do |t|
      t.attachment :video
    end
  end

  def self.down
    remove_attachment :replies, :video
  end
end
