class CreateStops < ActiveRecord::Migration
  def change
    create_table :stops do |t|
      t.string :text
      t.integer :score
      t.integer :question_id

      t.timestamps
    end
  end
end
