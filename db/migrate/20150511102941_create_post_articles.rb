class CreatePostArticles < ActiveRecord::Migration
  def change
    create_table :post_articles do |t|
      t.string :link
      t.string :title
      t.text :content

      t.timestamps
    end
  end
end
