class CreateTextBoxes < ActiveRecord::Migration
  def change
    create_table :text_boxes do |t|
      t.string :text
      t.integer :width
      t.integer :alpha
      t.timestamps
    end
  end
end
