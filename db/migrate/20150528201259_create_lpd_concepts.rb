class CreateLpdConcepts < ActiveRecord::Migration
  def change
    create_table :lpd_concepts do |t|
      t.string :name
      t.text :description
      t.string :video_url
      t.integer :position
      t.integer :leader_professional_development_id

      t.timestamps null: false
    end
  end
end
