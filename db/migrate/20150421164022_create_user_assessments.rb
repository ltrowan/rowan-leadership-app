class CreateUserAssessments < ActiveRecord::Migration
  def change
    create_table :user_assessments do |t|
      t.integer :user_id
      t.integer :assessment_id
      t.boolean :status

      t.timestamps
    end
  end
end
