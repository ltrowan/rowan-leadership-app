class AddUserIdIntoPostVideo < ActiveRecord::Migration
  def change
    add_column :post_videos, :user_id, :integer
  end
end
