class AddAttachmentBackgroundImageToTabs < ActiveRecord::Migration
  def self.up
    change_table :tabs do |t|
      t.attachment :background_image
    end
  end

  def self.down
    remove_attachment :tabs, :background_image
  end
end
