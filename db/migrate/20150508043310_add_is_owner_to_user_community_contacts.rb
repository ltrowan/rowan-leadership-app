class AddIsOwnerToUserCommunityContacts < ActiveRecord::Migration
  def change
    add_column :user_community_contacts, :is_owner, :boolean, default: false
  end
end
