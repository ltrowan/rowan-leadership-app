class RemoveColumn < ActiveRecord::Migration
  def change
    remove_column :content_block_posts, :content
    remove_column :content_block_posts, :parent_post_id
  end
end
