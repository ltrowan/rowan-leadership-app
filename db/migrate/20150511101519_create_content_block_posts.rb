class CreateContentBlockPosts < ActiveRecord::Migration
  def change
    create_table :content_block_posts do |t|
      t.integer :content_block_id
      t.integer :user_id
      t.integer :postable_id
      t.string :postable_type
      t.text :content
      t.timestamps
    end
  end
end
