class RenameUserContacts < ActiveRecord::Migration
  def change
    rename_table :user_contacts, :contacts_users
  end
end
