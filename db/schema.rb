# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150829182804) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_admin_comments", force: :cascade do |t|
    t.string   "namespace",     limit: 255
    t.text     "body"
    t.string   "resource_id",   limit: 255, null: false
    t.string   "resource_type", limit: 255, null: false
    t.integer  "author_id"
    t.string   "author_type",   limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "admin_users", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                      default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree

  create_table "assessments", force: :cascade do |t|
    t.string   "title",       limit: 255
    t.string   "anchor_name", limit: 255
    t.string   "sub_title",   limit: 255
    t.string   "time",        limit: 255
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "base_locations", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "state_id",   limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "capabilities", force: :cascade do |t|
    t.string   "sub_title",                     limit: 255
    t.string   "background_photo_file_name",    limit: 255
    t.string   "background_photo_content_type", limit: 255
    t.integer  "background_photo_file_size"
    t.datetime "background_photo_updated_at"
    t.text     "main_text"
    t.string   "type",                          limit: 255
    t.integer  "score_range_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "contacts_users", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "contact_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "content_block_posts", force: :cascade do |t|
    t.integer  "content_block_id"
    t.integer  "user_id"
    t.integer  "postable_id"
    t.string   "postable_type",    limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "community_id"
  end

  create_table "content_blocks", force: :cascade do |t|
    t.string   "title",                  limit: 255
    t.string   "block_type",             limit: 255
    t.integer  "block_id"
    t.string   "anchor_name",            limit: 255
    t.integer  "position"
    t.string   "url",                    limit: 255
    t.string   "quick_navigation_title", limit: 255
    t.integer  "spacing"
    t.integer  "section_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "countries", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "images", force: :cascade do |t|
    t.string   "pc_image_file_name",        limit: 255
    t.string   "pc_image_content_type",     limit: 255
    t.integer  "pc_image_file_size"
    t.datetime "pc_image_updated_at"
    t.string   "mobile_image_file_name",    limit: 255
    t.string   "mobile_image_content_type", limit: 255
    t.integer  "mobile_image_file_size"
    t.datetime "mobile_image_updated_at"
    t.integer  "tab_id"
    t.integer  "height"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "position",                              default: 1
  end

  create_table "introductions", force: :cascade do |t|
    t.integer  "width"
    t.integer  "alpha"
    t.string   "bg_color",    limit: 255
    t.integer  "position"
    t.text     "text"
    t.integer  "body_width"
    t.integer  "body_alpha"
    t.text     "body_text"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "space_upper", limit: 255
  end

  create_table "leader_professional_developments", force: :cascade do |t|
    t.string   "title"
    t.string   "subtitle"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.string   "background_image_file_name"
    t.string   "background_image_content_type"
    t.integer  "background_image_file_size"
    t.datetime "background_image_updated_at"
  end

  create_table "lpd_concepts", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.string   "video_url"
    t.integer  "position"
    t.integer  "leader_professional_development_id"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
  end

  create_table "lpds", force: :cascade do |t|
    t.string   "design",                             limit: 255
    t.text     "subject"
    t.string   "image_file_name",                    limit: 255
    t.string   "image_content_type",                 limit: 255
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.string   "subtitle",                           limit: 255
    t.string   "time",                               limit: 255
    t.string   "lpd_anchor_name",                    limit: 255
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "leader_professional_development_id"
  end

  create_table "master_templates", force: :cascade do |t|
    t.string   "name"
    t.string   "template_type"
    t.text     "description"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "media", force: :cascade do |t|
    t.string   "file_file_name",    limit: 255
    t.string   "file_content_type", limit: 255
    t.integer  "file_file_size"
    t.datetime "file_updated_at"
    t.integer  "assessment_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "post_articles", force: :cascade do |t|
    t.string   "link",       limit: 255
    t.string   "title",      limit: 255
    t.text     "content"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "post_images", force: :cascade do |t|
    t.string   "title",              limit: 255
    t.string   "image_file_name",    limit: 255
    t.string   "image_content_type", limit: 255
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "post_links", force: :cascade do |t|
    t.string   "url",         limit: 255
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "post_texts", force: :cascade do |t|
    t.text     "content"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "post_videos", force: :cascade do |t|
    t.string   "title",              limit: 255
    t.string   "video_file_name",    limit: 255
    t.string   "video_content_type", limit: 255
    t.integer  "video_file_size"
    t.datetime "video_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
  end

  create_table "questions", force: :cascade do |t|
    t.string   "title",         limit: 255
    t.integer  "stops_count"
    t.integer  "position"
    t.string   "intro_text",    limit: 255
    t.text     "question_text"
    t.integer  "assessment_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "quick_links", force: :cascade do |t|
    t.integer  "tab_id"
    t.integer  "position"
    t.string   "title",      limit: 255
    t.string   "url",        limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "ranks", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "rowgroup_id"
    t.integer  "order"
  end

  create_table "replies", force: :cascade do |t|
    t.integer  "content_block_post_id"
    t.text     "content"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "video_file_name"
    t.string   "video_content_type"
    t.integer  "video_file_size"
    t.datetime "video_updated_at"
  end

  create_table "score_ranges", force: :cascade do |t|
    t.integer  "start"
    t.integer  "ending"
    t.integer  "assessment_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "sections", force: :cascade do |t|
    t.string   "title",      limit: 255
    t.integer  "position"
    t.string   "time",       limit: 255
    t.integer  "tab_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "states", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.integer  "country_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "stops", force: :cascade do |t|
    t.string   "text",        limit: 255
    t.integer  "score"
    t.integer  "question_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "tab_links", force: :cascade do |t|
    t.string   "title"
    t.string   "url"
    t.integer  "tab_id"
    t.integer  "position"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tabs", force: :cascade do |t|
    t.string   "title",                         limit: 255
    t.string   "anchor_name",                   limit: 255
    t.string   "url",                           limit: 255
    t.string   "time",                          limit: 255
    t.float    "frame_size"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "background_image_file_name",    limit: 255
    t.string   "background_image_content_type", limit: 255
    t.integer  "background_image_file_size"
    t.datetime "background_image_updated_at"
    t.integer  "position"
  end

  create_table "taggings", force: :cascade do |t|
    t.integer  "tag_id"
    t.integer  "taggable_id"
    t.string   "taggable_type"
    t.integer  "tagger_id"
    t.string   "tagger_type"
    t.string   "context",       limit: 128
    t.datetime "created_at"
  end

  add_index "taggings", ["tag_id", "taggable_id", "taggable_type", "context", "tagger_id", "tagger_type"], name: "taggings_idx", unique: true, using: :btree
  add_index "taggings", ["taggable_id", "taggable_type", "context"], name: "index_taggings_on_taggable_id_and_taggable_type_and_context", using: :btree

  create_table "tags", force: :cascade do |t|
    t.string  "name"
    t.integer "taggings_count", default: 0
  end

  add_index "tags", ["name"], name: "index_tags_on_name", unique: true, using: :btree

  create_table "templates", force: :cascade do |t|
    t.string   "image_file_name",    limit: 255
    t.string   "image_content_type", limit: 255
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.integer  "width"
    t.integer  "alpha"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "text"
    t.integer  "master_template_id"
  end

  create_table "text_boxes", force: :cascade do |t|
    t.text     "text"
    t.integer  "width"
    t.integer  "alpha"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "titles", force: :cascade do |t|
    t.text     "text"
    t.string   "animation",     limit: 255
    t.integer  "width"
    t.string   "justification", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "ttps", force: :cascade do |t|
    t.string   "audio_file_name",    limit: 255
    t.string   "audio_content_type", limit: 255
    t.integer  "audio_file_size"
    t.datetime "audio_updated_at"
    t.text     "text"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "user_assessment_stops", force: :cascade do |t|
    t.string   "stop_id",    limit: 255
    t.text     "response"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "user_assessments", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "assessment_id"
    t.boolean  "status"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "user_communities", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "color",      limit: 255
    t.boolean  "status"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "user_community_contacts", force: :cascade do |t|
    t.integer  "user_community_id"
    t.integer  "contact_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "is_owner",          default: false
  end

  create_table "user_templates", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "master_template_id"
    t.string   "name"
    t.text     "template_data"
    t.string   "guid"
    t.boolean  "status"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.integer  "mentee_id"
  end

  create_table "users", force: :cascade do |t|
    t.string   "first_name",             limit: 255
    t.string   "last_name",              limit: 255
    t.integer  "rank_id"
    t.integer  "state_id"
    t.integer  "country_id"
    t.integer  "base_location_id"
    t.string   "miltary_email",          limit: 255
    t.string   "image_file_name",        limit: 255
    t.string   "image_content_type",     limit: 255
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                      default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.string   "confirmation_token",     limit: 255
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email",      limit: 255
    t.text     "image_data"
  end

  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["first_name", "last_name"], name: "index_users_on_first_name_and_last_name", using: :btree
  add_index "users", ["first_name"], name: "index_users_on_first_name", using: :btree
  add_index "users", ["last_name"], name: "index_users_on_last_name", using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "video_horizontals", force: :cascade do |t|
    t.integer  "width"
    t.integer  "alpha"
    t.boolean  "hide_text"
    t.string   "place_text",         limit: 255
    t.text     "text"
    t.integer  "video_width"
    t.string   "video_file_name",    limit: 255
    t.string   "video_content_type", limit: 255
    t.integer  "video_file_size"
    t.datetime "video_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "video_url",          limit: 255
  end

  create_table "video_statuses", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "video_type"
    t.integer  "content_block_post_id"
    t.boolean  "processed",             default: false
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
  end

  create_table "video_verticals", force: :cascade do |t|
    t.integer  "width"
    t.integer  "alpha"
    t.boolean  "hide_text"
    t.string   "place_text",         limit: 255
    t.text     "text"
    t.integer  "video_width"
    t.string   "video_file_name",    limit: 255
    t.string   "video_content_type", limit: 255
    t.integer  "video_file_size"
    t.datetime "video_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "video_url",          limit: 255
  end

end
