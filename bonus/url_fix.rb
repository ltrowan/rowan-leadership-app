tgt_string = "http://localhost:3000"
#tgt_string = "https://leadership.rowan.nyc"


ContentBlock.all.each do |tt|
  #tt.url.gsub!("https://rowantech-leadership-stage.herokuapp.com", "https://leadership.rowan.nyc") unless tt.url.nil?  
  tt.url.gsub!("https://rowantech-leadership-stage.herokuapp.com", tgt_string) unless tt.url.nil? 
end 

Tab.all.each do |tt|
  #tt.url.gsub!("https://rowantech-leadership-stage.herokuapp.com", "https://leadership.rowan.nyc") unless tt.url.nil?  
  tt.url.gsub!("https://rowantech-leadership-stage.herokuapp.com", tgt_string) unless tt.url.nil? 
end 

QuickLink.all.each do |tt|
  #tt.url.gsub!("https://rowantech-leadership-stage.herokuapp.com", "https://leadership.rowan.nyc") unless tt.url.nil?  
  tt.url.gsub!("https://rowantech-leadership-stage.herokuapp.com", tgt_string) unless tt.url.nil? 
end 
# Everything with the problematic polymorphic association. The 'text' attribute
# for each of these may or may not contain urls inline.
#app/models/introduction.rb:  has_one :content_block, :as => :block 
#app/models/lpd.rb:  has_one :content_block, :as => :block 
#app/models/template.rb:  has_one :content_block, :as => :block 
#app/models/text_box.rb:  has_one :content_block, :as => :block 
#app/models/title.rb:  has_one :content_block, :as => :block
#app/models/video_horizontal.rb:  has_one :content_block, :as => :block
#app/models/video_vertical.rb:  has_one :content_block, :as => :block

Introduction.all.each do |tt|
  tt.text.gsub!("https://rowantech-leadership-stage.herokuapp.com", tgt_string) unless tt.text.nil? 
  tt.save
end 

#Lpd.all.each do |tt|
#  tt.text.gsub!("https://rowantech-leadership-stage.herokuapp.com", tgt_string) unless tt.text.nil? 
#end 

Template.all.each do |tt|
  tt.text.gsub!("https://rowantech-leadership-stage.herokuapp.com", tgt_string) unless tt.text.nil? 
  tt.save
end 

TextBox.all.each do |tt|
  tt.text.gsub!("https://rowantech-leadership-stage.herokuapp.com", tgt_string) unless tt.text.nil? 
  tt.save
end 

Title.all.each do |tt|
  tt.text.gsub!("https://rowantech-leadership-stage.herokuapp.com", tgt_string) unless tt.text.nil? 
  tt.save
end 

VideoHorizontal.all.each do |tt|
  tt.text.gsub!("https://rowantech-leadership-stage.herokuapp.com", tgt_string) unless tt.text.nil? 
  tt.save
end 

VideoVertical.all.each do |tt|
  tt.text.gsub!("https://rowantech-leadership-stage.herokuapp.com", tgt_string) unless tt.text.nil? 
  tt.save
end 

