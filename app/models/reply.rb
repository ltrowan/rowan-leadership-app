class Reply < ActiveRecord::Base
  belongs_to :content_block_post
  belongs_to :user

  has_attached_file :video,
                    :url => ":basename.:extension",
                    :path => ":basename.:extension"

  validates_presence_of :user, :content_block_post
  validates_presence_of :content, unless: :video?
end
