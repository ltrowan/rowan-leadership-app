class Medium < ActiveRecord::Base
  belongs_to :assessment
  has_attached_file :file
  validates_attachment_content_type :file, 
    :content_type => /\Aimage|audio|video\/.*\Z/
  validates :file, presence: true
end
