class PostLink < ActiveRecord::Base
  has_one :post, :as => :postable

  validates_presence_of :url
end
