class TextBox < ActiveRecord::Base
  has_one :content_block, :as => :block 
  validates :text, presence: {message: "cannot be blank"} 
  validates :width, :alpha, numericality: { :greater_than_or_equal_to => 0 }
  
  def assign_params(params)
    self.text = params[:text]
    self.width = params[:width]
    self.alpha = params[:alpha]
  end
end
