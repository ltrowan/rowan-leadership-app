class Ttp < ActiveRecord::Base
  has_attached_file :audio  ,
                    :url => "/system/:class/:id/:attachment/:style.:extension",
                    :path => ":rails_root/public/system/:class/:id/:attachment/:style.:extension"
  validates_attachment_content_type :audio, :content_type => /\Aaudio\/.*\Z/
  validates :text, presence: {message: "cannot be blank"}
  
  def assign_params(params)
    self.audio = params[:audio] if !params[:audio].blank? 
    self.text = params[:text]
  end

end
