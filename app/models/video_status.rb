class VideoStatus < ActiveRecord::Base
  validates :user_id, :video_type, :content_block_post_id, presence: true
end
