class LpdConcept < ActiveRecord::Base
  belongs_to :leader_professional_development
  acts_as_list
  
  def lpd
  	self.leader_professional_development
  end

end
