class ContentBlockPost < ActiveRecord::Base

  belongs_to :postable, :polymorphic => true, dependent: :destroy
  belongs_to :user
  belongs_to :content_block
  belongs_to :user_community, foreign_key: :community_id
  has_many :replies, dependent: :destroy
  Types = [["PostText", 1], ["PostLink", 2], ["PostVideo", 3]]

  enum post_type: [:text, :link, :video]

  default_scope {order("created_at DESC")}

  def post
    postable
  end
end
