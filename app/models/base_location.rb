class BaseLocation < ActiveRecord::Base
  belongs_to :state
  has_many :users

  validates :name, presence: true, uniqueness: true
end
