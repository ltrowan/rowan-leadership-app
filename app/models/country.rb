class Country < ActiveRecord::Base
  has_many :states
  has_many :users

  validates :name, presence: true, uniqueness: true
end
