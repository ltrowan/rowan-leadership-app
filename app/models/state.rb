class State < ActiveRecord::Base
  has_many :users
  has_many :base_locations
  belongs_to :country

  validates :name, presence: true, uniqueness: true
end
