class Template < ActiveRecord::Base
  has_attached_file :image, :styles => { :thumb => "100x100>", :vertical => "400x400>"}, :default_url => "/images/:style/missing.png"
  validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/
  has_one :content_block, :as => :block 
  validates :image, presence: {message: "cannot be blank"}
  # validates :text, presence: {message: "cannot be blank"}
  validates :width, :alpha, numericality: { :greater_than_or_equal_to => 0 }
  belongs_to :master_template
  def assign_params(params)
    self.image = params[:image] if !params[:image].blank? 
    self.width = params[:width]
    self.alpha = params[:alpha]
    self.text = params[:text]
    self.master_template_id = params[:template_type]
  end
end
