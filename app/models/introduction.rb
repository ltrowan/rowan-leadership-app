class Introduction < ActiveRecord::Base
  has_one :content_block, :as => :block 
  validates :bg_color, :text, :body_text, presence: {message: "cannot be blank"}
  validates :position, :body_width, :body_alpha, :width, :alpha, :space_upper,
    numericality: { :greater_than_or_equal_to => 0 }
  
  def assign_params(params)
  	self.width = params[:width] 
  	self.alpha = params[:alpha] 
    self.bg_color = params[:bg_color]
    self.position = params[:position]  
    self.text = params[:text]  
    self.body_width = params[:body_width] 
    self.body_alpha = params[:body_alpha] 
    self.body_text = params[:body_text]
    self.space_upper = params[:space_upper]
  end
end