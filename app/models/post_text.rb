class PostText < ActiveRecord::Base
  has_one :post, :as => :postable

  validates_presence_of :content
end
