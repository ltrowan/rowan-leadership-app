class Section < ActiveRecord::Base
  #associations
  belongs_to :tab
  #acts_as_list scope: :tab
  has_many :content_blocks, -> { order("position ASC") }
  
  #validations
  validates :title, :time, presence: true

  #attributes
  accepts_nested_attributes_for :content_blocks
  
  def tab_sort
    self.tab.title
  end
 
end
