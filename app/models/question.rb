class Question < ActiveRecord::Base
  belongs_to :assessment
  has_many :stops
  acts_as_list
  validates :title, presence: true
  validates :position, numericality: true
end
