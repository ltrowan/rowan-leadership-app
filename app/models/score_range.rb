class ScoreRange < ActiveRecord::Base
  belongs_to :assessment
  validates :start, :ending, numericality: true
  validates :start, :ending, presence: true
  has_many :capabilities
  accepts_nested_attributes_for :capabilities, :allow_destroy => true
  
  def range
    return "#{self.start} - #{self.ending}"
  end
  
  def self.save_capability( params)
    score_range = ScoreRange.find(params[:id])
    capabilities = score_range.capabilities
    error = false
    if params[:study].blank?
      study = capabilities.study.new
    else
      study = Capability.find( params[:study] )
    end
    study.main_text = params[:study_main_text]
    study.sub_title = params[:study_sub_title]
    study.background_photo = params[:study_image] if !params[:study_image].blank? 
    if study.valid?
      study.save
    else
    	error = true
    end
    if params[:practice].blank?
      practice = capabilities.practice.new
    else
      practice = Capability.find( params[:practice] )
    end
    practice.main_text = params[:practice_main_text]
    practice.sub_title = params[:practice_sub_title]
    practice.background_photo = params[:practice_image] if !params[:practice_image].blank? 
    if practice.valid?
      practice.save
    else
    	error = true
    end
    if params[:gb].blank?
      gb = capabilities.gather_feedback.new
    else
      gb = Capability.find( params[:gb] )
    end
    gb.main_text = params[:gb_main_text]
    gb.sub_title = params[:gb_sub_title]
    gb.background_photo = params[:gb_image] if !params[:gb_image].blank? 
    if gb.valid?
      gb.save
    else
    	error = true
    end
    return study, practice, gb, error
  end
end
