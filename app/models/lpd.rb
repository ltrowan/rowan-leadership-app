class Lpd < ActiveRecord::Base
  has_attached_file :image, :styles => { :thumb => "100x100>", :vertical => "400x400>"}, :default_url => "/images/:style/missing.png"
  validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/
  has_one :content_block, :as => :block 
  belongs_to :leader_professional_development

  validates :design, :subject, :subtitle, :time, :description,
    presence: {message: "cannot be blank"}
  
  def assign_params(params)
    self.design = params[:design] 
    self.subject = params[:subject]  
    self.image = params[:image] if !params[:image].blank? 
    self.subtitle = params[:subtitle] 
    self.time = params[:time] 
    self.description = params[:description]
    self.lpd_anchor_name = params[:lpd_anchor_name]
    self.leader_professional_development_id = params[:lpd]
  end
end
