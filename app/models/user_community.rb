class UserCommunity < ActiveRecord::Base
  has_many :user_community_contacts, dependent: :destroy
  has_many :contacts, through: :user_community_contacts, source: :contact
  belongs_to :user
  validates :name, :color, presence: true
  has_many :content_block_posts, foreign_key: :community_id

  def owner
    self.user
  end
  
  def active
    self.status
  end

end
