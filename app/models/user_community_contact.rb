class UserCommunityContact < ActiveRecord::Base
  belongs_to :contact, foreign_key: 'contact_id', class_name: 'User'
  belongs_to :user_community
end
