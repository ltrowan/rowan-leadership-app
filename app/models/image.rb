class Image < ActiveRecord::Base
  belongs_to :tab
  acts_as_list
  
  has_attached_file :pc_image, 
    :styles => { :thumb => "100x100>", :vertical => "200x400>"}, 
    :default_url => "/images/:style/missing.png"
  validates_attachment_content_type :pc_image, 
    :content_type => /\Aimage\/.*\Z/

  has_attached_file :mobile_image, 
    :styles => { :thumb => "100x100>", :vertical => "200x400>"}, 
    :default_url => "/images/:style/missing.png"
  validates_attachment_content_type :mobile_image, 
    :content_type => /\Aimage\/.*\Z/
  validates :height, numericality: { :greater_than_or_equal_to => 0 }

end
