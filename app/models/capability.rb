class Capability < ActiveRecord::Base
  belongs_to :score_range
  self.inheritance_column = :type
  scope :study, -> { where(type: 'Study') } 
  scope :practice, -> { where(type: 'Practice') } 
  scope :gather_feedback, -> { where(type: 'GatherFeedback') } 
  
  validates :sub_title, presence: true
  
  has_attached_file :background_photo, 
    :styles => { :thumb => "100x100>"}, 
    :default_url => "/images/:style/missing.png"
  validates_attachment_content_type :background_photo, 
    :content_type => /\Aimage\/.*\Z/
  def self.types
    %w('GatherFeedback' 'Study' 'Practice')
  end
end
