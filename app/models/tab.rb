class Tab < ActiveRecord::Base
  #associations
  has_many :quick_links, -> { order("position ASC") }
  accepts_nested_attributes_for :quick_links, :allow_destroy => true
  
  has_many :sections, -> { order("position ASC") }
  accepts_nested_attributes_for :sections, :allow_destroy => true
  
  has_many :images, -> { order("position ASC") }
  accepts_nested_attributes_for :images
  
  has_many :tab_links, -> { order("position ASC") }
  accepts_nested_attributes_for :tab_links, :allow_destroy => true
  #validations  
  validates :frame_size, :position, numericality: true
  validates :anchor_name, uniqueness: true, format: { with: /\A[a-zA-Z0-9]+\z/, message: "only allows letters or numbers" }
  validates :title,:anchor_name, :time, :frame_size, 
    :description,  presence: true
  
  #validates :url, url: true
  
  #attributes
  has_attached_file :background_image, 
    :styles => { :thumb => "100x100>", :vertical => "200x400>"}, 
    :default_url => "/images/:style/missing.png"
  validates_attachment_content_type :background_image, 
    :content_type => /\Aimage\/.*\Z/

end
