class ContentBlock < ActiveRecord::Base
  #associations
  belongs_to :section
  belongs_to :block, :polymorphic => true
  has_many :content_block_posts
  acts_as_list #scope: :section
  #validations
  validates :title, :anchor_name,  
    :quick_navigation_title, :spacing, presence: {message: "cannot be blank"} 

  validates :spacing, numericality: { :greater_than_or_equal_to => 0 }
  #validates :url, url: true
  acts_as_ordered_taggable

  def self.save_block(params)
    if params[:id]
      @content_block = ContentBlock.find(params[:id])
      type = @content_block.block
    else  
      @content_block = ContentBlock.new
      case params[:type].to_i
        when AdminData::Block::TextBox
          type = TextBox.new
        when AdminData::Block::Title
          type = Title.new    
        when AdminData::Block::Template
          type = Template.new
        when AdminData::Block::VideoHorizontal
          type = VideoHorizontal.new
        when AdminData::Block::VideoVertical
          type = VideoVertical.new
        when AdminData::Block::Ttp
          type = Ttp.new
        when AdminData::Block::Introduction
          type = Introduction.new
        when AdminData::Block::Lpd
          type = Lpd.new
        else
      end
       
    end

   type.assign_params(params)
    if type.valid?
      if type.save
        @content_block = assign_params(@content_block, type, params)
        if @content_block.valid?
          @content_block.save
        end
      else
        @content_block.valid?
        @content_block.errors.messages.merge!(type.errors.messages)
      end
    else
      @content_block = assign_params(@content_block, type, params)
      @content_block.valid?
      @content_block.errors.messages.merge!(type.errors.messages)
    end
    return @content_block, type
  end
  
  def self.assign_params(content_block, type, params)
    content_block.block = type
    content_block.title = params[:title]
    content_block.anchor_name = params[:anchor_name]
    content_block.quick_navigation_title = params[:quick_navigation_title]
    content_block.url = params[:url]
    content_block.spacing = params[:spacing] 
    content_block.section_id = params[:section_id] 
    if !["2", "3", "7"].include? type
      content_block.tag_list = params[:tag_list]
    end
    content_block
  end
end