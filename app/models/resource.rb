class Resource < ActiveRecord::Base
   has_attached_file :image, :styles => { :thumb => "100x100>", :vertical => "400x400>"}, :default_url => "/images/:style/missing.png"
  validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/
end
