class Assessment < ActiveRecord::Base
  acts_as_ordered_taggable
  has_many :score_ranges
  validates :title, :sub_title, :anchor_name, :time, :description,
    presence: {message: "cannot be blank"}
  has_many :questions
  has_many :media
end
