class LeaderProfessionalDevelopment < ActiveRecord::Base
  has_many :lpd_concepts
  has_many :lpds
  has_attached_file :background_image, 
    :default_url => "/images/:style/missing.png"
  validates_attachment_content_type :background_image, 
    :content_type => /\Aimage\/.*\Z/

  def content_blocks
    self.lpds
  end
  def concepts
  	self.lpd_concepts
  end

end
