class User < ActiveRecord::Base
  default_scope {includes(:state)}
  default_scope {includes(:country)}
  default_scope {includes(:rank)}
  default_scope {includes(:base_location)}
  
  has_many :contacts_users
  has_many :contacts, through: :contacts_users, class_name: 'User'

  has_many :user_community_contacts, foreign_key: :contact_id
  has_many :user_communities, dependent: :destroy
  has_many :communities, through: :user_community_contacts, source: :user_community
  has_many :content_block_posts, dependent: :destroy
  has_many :templates, class_name: 'UserResourceTemplate'
  has_many :template_types, through: :templates, class_name: 'ResourceTemplate'
  
  #has_many :user_communities, :through => :user_communities_contacts
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  belongs_to :state
  belongs_to :country
  belongs_to :rank
  belongs_to :base_location
  devise :database_authenticatable, :registerable,
    :recoverable, :rememberable, :trackable, :validatable#, :confirmable
   
  validates :first_name, :last_name, :rank_id, :country_id, :state_id, :base_location_id, presence: true
  has_attached_file :image, :styles => { :thumb => "100x100>", :large => "500x500>"}, :default_url => "missing_user.jpeg",  processors: [:cropper]
  validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/
  searchkick word_start: [:first_name, :last_name]

  attr_accessor :crop_x, :crop_y, :crop_w, :crop_h
  # after_update :reprocess_image, :if => :cropping?

  scope :search_name, -> (query) { where(
      "LOWER(first_name) like ? or LOWER(last_name) like ? or LOWER(CONCAT(first_name, ' ', last_name)) like ?",
      "%#{query.downcase}%", "%#{query.downcase}%", "%#{query.downcase}%"
  )}

  
  def search_data
    attributes.merge(
      image_url: image.url
    )
  end
  
  def image_url
    self.image.url
  end
  
  def name
    "#{first_name} #{last_name}".titleize
  end

  def rank_last_name
    "#{rank.name} #{last_name}"
  end

  def rank_full_name
    "#{rank.name} #{first_name} #{last_name}"
  end

  def cropping?
    !crop_x.blank? && !crop_y.blank? && !crop_w.blank? && !crop_h.blank?
  end

  def image_geometry(style = :original)
    @geometry ||= {}
    @geometry[style] ||= Paperclip::Geometry.from_file(image.url(style))
  end



  def reprocess_image
    self.image.reprocess!
    self.save
  end



  #def member_communities
   # UserCommunityContact.where(contact_id: self.id).map{|com| com.user_community }
  #end
end
