class TabLink < ActiveRecord::Base
  acts_as_ordered_taggable
  belongs_to :tab
end
