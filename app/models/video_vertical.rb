class VideoVertical < ActiveRecord::Base
  has_one :content_block, :as => :block
  has_attached_file :video  ,
                    :url => "/system/:class/:id/:attachment/:style.:extension",
                    :path => ":rails_root/public/assets/:class/:id/:attachment/:style.:extension"
  validates_attachment_content_type :video, :content_type => /\Avideo\/.*\Z/
  validates :text, presence: {message: "cannot be blank"}
  
  validates :width, :alpha, :video_width, numericality: { :greater_than_or_equal_to => 0 }
  
  def assign_params(params)
    self.width = params[:width]
    self.alpha = params[:alpha]
    self.text = params[:text] 
    self.hide_text = params[:hide_text] 
    self.place_text = params[:place_text] 
    self.video_width = params[:video_width] 
    self.video_url = params[:video] if !params[:video].blank? 
  end
end
