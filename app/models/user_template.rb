class UserTemplate < ActiveRecord::Base
	belongs_to :user
  belongs_to :master_template

  before_save :set_mentee

  def title
    JSON.parse(template_data)["title"] rescue nil
  end


  private
  def set_mentee
    _mentee_id = JSON.parse(template_data)[:mentee][:originalObject][:id] rescue nil
    self.mentee_id = _mentee_id if _mentee_id.present?
  end

end
