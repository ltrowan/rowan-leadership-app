class QuickLink < ActiveRecord::Base
  belongs_to :tab
  #acts_as_list scope: :tab
  #validates :url, url: true
  validates :title, presence: true
end
