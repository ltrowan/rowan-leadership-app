class MasterTemplate < ActiveRecord::Base
  EXCLUDE_TEMPLATES =  %w(TIGER_TEAM_IMPLEMENTATION_TOOL UNIT_LEADER_DEVELOPMENT_ROLES_AND_RESPONSIBILITIES COMMAND_UNIT_LEADER_DEVELOPMENT_SCORECARD)

	acts_as_ordered_taggable
	has_many :user_templates
	has_many :users, through: :user_templates
  has_many :templates
  validates :name, :template_type, presence: true, uniqueness: true

  scope :exclude_templates, -> { where.not(template_type: EXCLUDE_TEMPLATES) }
end
