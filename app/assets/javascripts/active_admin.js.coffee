#= require active_admin/base
#= require tinymce
#= require common
#= require z.jquery.fileupload


$(document).ready ->
  tinymce.init
	  selector: 'textarea:not(.mceNoEditor)'
	  plugins: [
	    'advlist autolink autosave link image lists charmap print preview hr anchor pagebreak spellchecker'
	    'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking'
	    'table contextmenu directionality emoticons template textcolor paste fullpage textcolor'
	  ]
	  toolbar1: 'bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect'
	  toolbar2: 'cut copy paste | bullist numlist | outdent indent blockquote | undo redo | anchor | forecolor backcolor'
	  menubar: false
	  toolbar_items_size: 'small'
	  height: '100'
	  width: 500
  return