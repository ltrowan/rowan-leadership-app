rowan.resourceTemplate.filter('selectedContactFilter', function() {
  return function(items, selected_contacts) {
    var out = [];

    if (angular.isArray(items)) {
      items.forEach(function(item) {
        if(selected_contacts.indexOf(item.id) <0){
          out.push(item);
        }
      });
    } else {
      out = items;
    }
    return out;
  };
});