rowan.resourceTemplate.service('userService',["$http", "$q", function ($http, $q){
    return {
        saveUserTemplate : function(template_type,template,user_template_id) {
            var deferred = $q.defer();
            //service for outline data
            $http.post("/templates/save.json",{type: template_type,
                                            template_data:template, 
                                            user_template_id: user_template_id}).then(function (response){
                deferred.resolve(response.data);
            }, onError);

            var onError = function (errorMessage) {
                alert('Failed: ' + errorMessage);
            };
            return deferred.promise;
        },
        saveUserCommunity : function(community){
          var deferred = $q.defer();
          $http.post("/user/community/save.json",{community: community}).then(function (response){
              deferred.resolve(response.data);
          }, onError);

          var onError = function (errorMessage) {
              alert('Failed: ' + errorMessage);
          };
          return deferred.promise;
        },
        deleteUserCommunity : function(community_id){
          var deferred = $q.defer();
          $http.get("/user/community/"+community_id+"/delete.json").then(function (response){
              deferred.resolve(response.data);
          }, onError);

          var onError = function (errorMessage) {
              alert('Failed: ' + errorMessage);
          };
          return deferred.promise;
        },
        deleteCommunityContact : function(community_id,user_id){
          var deferred = $q.defer();
          $http.get("/user/community/"+community_id+"/contact/"+user_id+"/delete.json").then(function (response){
              deferred.resolve(response.data);
          }, onError);

          var onError = function (errorMessage) {
              alert('Failed: ' + errorMessage);
          };
          return deferred.promise;
        },
        addCommunityContact : function(community_id,user_id){
          var deferred = $q.defer();
          $http.get("/user/community/"+community_id+"/contact/"+user_id+"/save.json").then(function (response){
              deferred.resolve(response.data);
          }, onError);

          var onError = function (errorMessage) {
              alert('Failed: ' + errorMessage);
          };
          return deferred.promise;
        },
        saveAllCommunity : function(all_coomunities){
          var deferred = $q.defer();
          $http.post("/user/allcommunity/save.json",{communities: all_coomunities}).then(function (response){
              deferred.resolve(response.data);
          }, onError);

          var onError = function (errorMessage) {
              alert('Failed: ' + errorMessage);
          };
          return deferred.promise;
        },
        searchContacts : function(key_word,country_id,state_id,base_id,rank_id){
          var deferred = $q.defer();
          //service for outline data
          $http.get("/search_all_users.json?query="+key_word+"&country_id="+country_id+"&state_id="+state_id+"&base_location_id="+base_id+"&rank_id="+rank_id).then(function (response){
             deferred.resolve(response.data);
          }, onError);

          var onError = function (errorMessage) {
              alert('Failed: ' + errorMessage);
          };
          return deferred.promise;
        },
        addContacts : function(user_id,contact_ids){
          var deferred = $q.defer();
          $http.post("/users/"+user_id+"/save_contacts.json",{contact_ids: contact_ids}).then(function (response){
              deferred.resolve(response.data);
          }, onError);

          var onError = function (errorMessage) {
              alert('Failed: ' + errorMessage);
          };
          return deferred.promise;
        },
        sendTemplateToContacts: function(template_id,contacts){
          var deferred = $q.defer();
          $http.post("/templates/send.json",{user_template_id: template_id,contacts: contacts}).then(function (response){
              deferred.resolve(response.data);
          }, onError);

          var onError = function (errorMessage) {
              alert('Failed: ' + errorMessage);
          };
          return deferred.promise;
        }
    }
}]);