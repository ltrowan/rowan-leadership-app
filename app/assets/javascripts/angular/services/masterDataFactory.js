rowan.resourceTemplate.service('masterDataFactory',[function(){
	return {
		template_definition : function(template_type){
            switch (template_type) {
                case "BE_A_MENTOR": //text box
                    return this.beAMentor;
                    break;
                case "COMMAND_UNIT_LEADER_DEVELOPMENT_SCORECARD":
                    return this.commandUnitLeaderDevelopmentScorecard;
                    break;
                case "DAILY_LEADER_OBSERVATIONS_PLANNER":
                    return this.dailyLeaderObservationsPlanner;
                    break;
                case "ASSIGNMENT_DEMANDS_ASSESSMENT":
                    return this.assignmentDemandsAssessment;
                    break;
                case "GETTING_TO_KNOW_TOOL":
                    return this.gettingToKnowTool;
                    break;
                case "INDIVIDUAL_PLANS_FOR_ADVANCEMENT":
                    return this.individualPlansForAdvancement;
                    break;
                case "LEADERSHIP_SELECTION_GUIDANCE":
                    return this.leadershipSelectionGuidance;
                    break;
                case "ASSESSING_CURRENT_LEARNING_INTERVENTIONS":
                    return this.assessingCurrentLearningInterventions;
                    break;
                case "LEVERAGE_ROLE_MODELS":
                    return this.leverageRoleModels;
                    break;
                case "TRAINING_AND_PROFESSIONAL_DEVELOPMENT":
                    return this.trainingAndProfessionalDevelopment;
                    break;
                case "SOAR_OBSERVATION_SOURCE":
                    return this.soarObservationSource;
                    break;
                case "STUDY_TOOL":
                    return this.studyTool;
                    break;
                case "TIGER_TEAM_IMPLEMENTATION_TOOL":
                    return this.tigerTeamImplementationTool;
                    break;
                case "UNIT_LEADER_DEVELOPMENT_ROLES_AND_RESPONSIBILITIES":
                    return this.unitLeaderDevelopmentRolesAndResponsibilities;
                    break;
            }
		},
		beAMentor : {
	    template_type : 'BE_A_MENTOR'.toLowerCase(),
	    pages: [
	        {
	            number: 1,
	            template_name: "",
	            date: ""
	        },
	        {
	            number: 2,
	            current_position: ""
	        },
	        {
	            number: 3,
	            questions: [
	                {
	                    text: "Candid feedback about perceived strengths and developmental needs",
	                    status: 0
	                },
	                {
	                    text: "Encouragement and motivation",
	                    status: 0
	                },
	                {
	                    text: "Advice on dealing with roadblocks",
	                    status: 0
	                },
	                {
	                    text: "Guidance on setting goals and periodically reviews progress",
	                    status: 0
	                }
	            ]
	        },
	        {
	            number: 4,
	            questions: [
	                {
	                    text: "Experiences that contributed to his/her own success",
	                    status: 0
	                },
	                {
	                    text: "A understanding of the Army, it’s mission, and formal and informal operating processes",
	                    status: 0
	                }
	            ]
	        },
	        {
	            number: 5,
	            questions: [
	                {
	                    text: "More efficient and productive performance",
	                    status: 0
	                },
	                {
	                    text: "Appropriate training and developmental opportunities",
	                    status: 0
	                },
	                {
	                    text: "Sense of self-awareness, self-confidence, and adaptability",
	                    status: 0
	                }
	            ]
	        },
	        {
	            number: 6,
	            questions: [
	                {
	                    text: "As a confidant, counselor, guide,",
	                    status: 0
	                },
	                {
	                    text: "As a sounding board for career development ideas/ opportunities",
	                    status: 0
	                },
	                {
	                    text: "As a resource for enhancing personal and professional attributes",
	                    status: 0
	                }
	            ]
	        },
	        {
	            number: 7,
                questions:[
                    {   text: "What is your planned action for becoming a mentor or maintaining a mentoring relationship?",
                        answer: ""
                    }
                ]

	        }
	    ],
	    current_page: 1,
	    mentee: {},
        title: ""

	    },
        commandUnitLeaderDevelopmentScorecard:{

            template_type : 'COMMAND_UNIT_LEADER_DEVELOPMENT_SCORECARD'.toLowerCase(),
            pages: [
                {
                    number: 1,
                    template_name: "",
                    date: ""
                },
                {
                    number: 2
                },
                {
                    number: 3,
                    questions: [
                        {
                            text: "A subordinate leader shared a challenge they are experiencing with you",
                            status: 0,
                            answer: ""
                        },
                        {
                            text: "Leader(s) expressed interest in joining your unit",
                            status: 0,
                            answer: ""
                        },
                        {
                            text: "Leader(s) expressed a desire to stay in your unit",
                            status: 0,
                            answer: ""
                        },
                        {
                            text: "Other units requested a leader from your unit",
                            status: 0,
                            answer: ""
                        }
                    ]
                },
                {
                    number: 4,
                    questions: [
                        {
                            text: "Unsolicited Soldier comments about their leaders",
                            status: 0,
                            answer: ""
                        },
                        {
                            text: "A new idea/innovation was implemented in your command",
                            status: 0,
                            answer: ""
                        },
                        {
                            text: "All key leader positions are filled/occupied",
                            status: 0,
                            answer: ""
                        },
                        {
                            text: "Multiple qualified candidates competed for last leadership position vacancy",
                            status: 0,
                            answer: ""
                        }
                    ]
                },
                {
                    number: 5,
                    questions: [
                        {
                            text: "Last leader with option to leave the Army was retained",
                            status: 0,
                            answer: ""
                        },
                        {
                            text: "Unit leader changes have little or no detrimental effect on unit performance",
                            status: 0,
                            answer: ""
                        },
                        {
                            text: "Initial performance of new leaders is high",
                            status: 0,
                            answer: ""
                        },
                        {
                            text: "Overall unit performance is high; no sub-unit is a consistent low performer",
                            status: 0,
                            answer: ""
                        },
                        {
                            text: "Leaders and their units demonstrate lessons learned; few repeat mistakes",
                            status: 0,
                            answer: ""
                        }
                    ]
                }

            ],
            current_page: 1,
            mentee: {},
            title: ""

        },
        dailyLeaderObservationsPlanner: {

            template_type : 'DAILY_LEADER_OBSERVATIONS_PLANNER'.toLowerCase(),
            pages: [
                {
                    number: 1,
                    template_name: "",
                    date: ""
                },
                {
                    number: 2

                },

                {
                    number: 3,
                    most_productive_times: [
                        {
                            date: "",
                            observation_window: "",
                            leader: {},
                            situation_to_observe: ""
                        },
                        {
                            date: "",
                            observation_window: "",
                            leader: {},
                            situation_to_observe: ""
                        },
                        {
                            date: "",
                            observation_window: "",
                            leader: {},
                            situation_to_observe: ""
                        },
                        {
                            date: "",
                            observation_window: "",
                            leader: {},
                            situation_to_observe: ""
                        }
                    ]
                }


            ],
            current_page: 1,
            mentee: {},
            title: ""

        },
        assignmentDemandsAssessment : {
        template_type: 'ASSIGNMENT_DEMANDS_ASSESSMENT'.toLowerCase(),
        pages: [
            {
                number: 1
            },
            {
                number: 2                
            },
            {
                number: 3,
                questions: [
                    {
                        title: "Turn around:",
                        text: "Unit/team has suffered a set-back and is poerforming poorly.",
                        is_part_of_job: false,
                        status: 0,
                        answer: ""
                    },
                    {
                        title: "High Performance Culture:",
                        text: "Expectations of leaders are high.",
                        is_part_of_job: false,
                        status: 0,
                        answer: ""
                    },
                    {
                        title: "Innovation:",
                        text: "Highly skilled team responsible for new ideas.",
                        is_part_of_job: false,
                        status: 0,
                        answer: ""
                    },
                    {
                        title: "Task Force/Matrix:",
                        text: "Leaders must influence people over whom they have no authority.",
                        is_part_of_job: false,
                        status: 0,
                        answer: ""
                    }
                ]
            },
            {
                number: 4,
                questions: [
                    {
                        title: "Cross-Cultural Situation:",
                        text: "Differing cultures must be influenced to achieve mission success.",
                        is_part_of_job: false,
                        status: 0,
                        answer: ""
                    },
                    {
                        title: "Staff Assignment - Direct:",
                        text: "Executes systems, procedures, and policies in support of unit.",
                        is_part_of_job: false,
                        status: 0,
                        answer: ""
                    },
                    {
                        title: "Staff Assignment - Operational/Strategic:",
                        text: "Designs and executes systems, procedures, and policies.",
                        is_part_of_job: false,
                        status: 0,
                        answer: ""
                    }
                ]
            },
            {
                number: 5,
                questions: [
                    {
                        title: "Confront Poor Performance:",
                        text: "Need to improve or replace subordinates.",
                        is_part_of_job: false,
                        status: 0,
                        answer: ""
                    },
                    {
                        title: "Achieve Results:",
                        text: "Definable, demanding performance measures.",
                        is_part_of_job: false,
                        status: 0,
                        answer: ""
                    },
                    {
                        title: "Additional Assignment Demands:",
                        text: "",
                        custom_question: true,
                        is_part_of_job: false,
                        status: 0,
                        answer: ""
                    }
                ]
            }
        ],
        current_page: 1,
        mentee: {
            
        },
        title: ""
    },
        gettingToKnowTool: {

            template_type : 'GETTING_TO_KNOW_TOOL'.toLowerCase(),
            pages: [
                {
                    number: 1,
                    template_name: "",
                    date: ""
                },
                {
                    number: 2
                },
                {
                    number: 3,
                    questions: [
                        {
                            text: "Professional goals and priorities",
                            answer: ""
                        },
                        {
                            text: "Key challenges in current assignment",
                            answer: ""
                        },
                        {
                            text: "Personal leadership strengths and development needs",
                            answer: ""
                        }
                    ]
                },
                {
                    number: 4,
                    questions: [
                        {
                            text: "Satisfaction with work and the Army profession",
                            answer: ""
                        },
                        {
                            text: "Relationship with peers and subordinates",
                            answer: ""
                        },
                        {
                            text: "Feelings about supervisors and our various work relationships",
                            answer: ""
                        }
                    ]
                },
                {
                    number: 5,
                    questions: [
                        {
                            text: "Previous assignments and career experiences",
                            answer: ""
                        },
                        {
                            text: "Important family members",
                            answer: ""
                        },
                        {
                            text: "Housing location and transportation to work",
                            answer: ""
                        }
                    ]
                },
                {
                    number: 6,
                    questions: [
                        {
                            text: "Unique skills (beyond military)",
                            answer: ""
                        },
                        {
                            text: "Main interests outside of work",
                            answer: ""
                        },
                        {
                            text: "Personal stressors",
                            answer: ""
                        }
                    ]
                },
                {
                    number: 7,
                    questions: [
                        {
                            text: "Any additional comments or thoughts",
                            answer: ""
                        }
                    ]
                }

            ],
            current_page: 1,
            mentee: {},
            title: ""

        },
        individualPlansForAdvancement: {
            template_type: 'INDIVIDUAL_PLANS_FOR_ADVANCEMENT'.toLowerCase(),
            pages: [
                {
                    number: 1
                },
                {
                    number: 2
                },
                {
                    number: 3,
                    responsibilities:[
                        {
                            text: "",
                            readiness: "1"
                        },
                        {
                            text: "",
                            readiness: "1"
                        },
                        {
                            text: "",
                            readiness: "1"
                        }
                    ],
                    questions: [
                        {
                            text: "Leader with Intellectual Capacity",
                            answer: ""
                        }
                    ]
                },
                {
                    number: 4,
                    questions: [
                        {

                            text: "Plans/Actions to Address Development Areas:",
                            answer: ""
                        },
                        {

                            text: "Next or Potential Position:",
                            answer: ""
                        }
                    ]
                },
                {
                    number: 5,
                    responsibilities:[
                        {
                            text: "",
                            readiness: "1"
                        },
                        {
                            text: "",
                            readiness: "1"
                        },
                        {
                            text: "",
                            readiness: "1"
                        }
                    ]
                },
                {
                    number: 6,
                    questions: [
                        {

                            text: "Plans/Actions to Prepare Leader for This Position",
                            answer: ""
                        }
                    ]
                }
            ],
            current_page: 1,
            mentee: {

            },
            title: ""
        },
        leadershipSelectionGuidance : {
            template_type : 'LEADERSHIP_SELECTION_GUIDANCE'.toLowerCase(),
            pages: [
                {
                    number: 1,
                    template_name: "",
                    date: ""
                },
                {
                    number: 2,
                    current_position: ""
                },
                {
                    number: 3,
                    questions: [
                        {
                            text: "Position to be filled:",
                            answer: ""
                        },
                        {
                            text: "Fill by (Date):",
                            answer: ""
                        },
                        {
                            text: "Stays in the job through (Date):",
                            status: ""
                        }
                    ]
                },
                {
                    number: 4,
                    questions: [
                        {
                            text: "Leader Requirements (write in guidance as to what is important for this leadership position)",
                            status: false
                        },
                        {
                            text: "Leader of Character:",
                            answer: ""
                        },
                        {
                            text: "Leader of Presence:",
                            answer: ""
                        },
                        {
                            text: "Leader with Intellectual Capacity:",
                            answer: ""
                        }
                    ]
                },
                {
                    number: 5,
                    questions: [
                        {
                            text: "Leader Requirements (write in guidance as to what is important for this leadership position)",
                            status: false
                        },
                        {
                            text: "Leads:",
                            answer: ""
                        },
                        {
                            text: "Develops:",
                            answer: ""
                        },
                        {
                            text: "Achieves:",
                            answer: ""
                        }
                    ]
                },
                {
                    number: 6,
                    questions: [
                        {
                            text: "Previous Leadership Experience/Training/Education (from Officer Record Brief ORB/DA Form 2-1)",
                            status: false
                        },
                        {
                            text: "Work Experience:",
                            answer: ""
                        },
                        {
                            text: "Training/Certifications:",
                            answer: ""
                        },
                        {
                            text: "Education:",
                            answer: ""
                        }
                    ]
                },
                {
                    number: 7,
                    questions: [
                        {
                            text: "Performance Assessment & Reference Check",
                            status: false
                        },
                        {
                            text: "Previous OER/NCOER performance needed for success in this position :",
                            answer: ""
                        },
                        {
                            text: "Previous Supervisor Recommendation:",
                            answer: ""
                        },
                        {
                            text: "Previous Senior Rater Recommendation:",
                            answer: ""
                        }
                    ]
                },
                {
                    number: 8,
                    questions: [
                        {
                            text: "Sample Job Capabilities",
                            status: false
                        },
                        {
                            text: "Task to Assess:",
                            answer: ""
                        },
                        {
                            text: "Proficiency level needed:",
                            answer: ""
                        }
                    ]
                }
            ],
            current_page: 1,
            mentee: {},
            title: ""

        },
    assessingCurrentLearningInterventions: {

            template_type : 'ASSESSING_CURRENT_LEARNING_INTERVENTIONS'.toLowerCase(),
            pages: [
                {
                    number: 1,
                    template_name: "",
                    date: ""
                },
                {
                    number: 2
                },
                {
                    number: 3,
                    questions: [
                        {
                            text: "Is the tempo of learning maintaining learner attention?",
                            answer: ""
                        },
                        {
                            text: "Are the physical conditions (temperature, lighting) sufficient to maintain learner attention?",
                            answer: ""
                        },
                        {
                            text: "Are the learning media (role play, rehearse/practice, simulation, discussion, lecture) appropriate for the task to be learned?",
                            answer: ""
                        },
                        {
                            text: "Do the content and standard for learning challenge/stretch learners beyond their current level of skill and experience?",
                            answer: ""
                        }
                    ]
                },
                {
                    number: 4,
                    questions: [
                        {
                            text: "Is the learning highly relevant/will it have an impact on unit performance? On improving leaders for the larger Army?",
                            answer: ""
                        },
                        {
                            text: "Are the experiences of leaders at the learning objective drawn upon?",
                            answer: ""
                        },
                        {
                            text: "Are leaders encouraged to bring up their experiences during the learning? Are these experiences integrated into the learning?",
                            answer: ""
                        }
                    ]
                },
                {
                    number: 5,
                    questions: [
                        {
                            text: "Do leaders look forward to learning opportunities?",
                            answer: ""
                        },
                        {
                            text: "In spite of challenges/limitations, do leaders seek to make the best of a learning opportunity?",
                            answer: ""
                        },
                        {
                            text: "Are leaders dissatisfied with their present level of learning and always looking to improve and learn?",
                            answer: ""
                        }
                    ]
                },
                {
                    number: 6,
                    questions: [
                        {
                            text: "Does learning include sufficient repetition and practice?",
                            answer: ""
                        },
                        {
                            text: "Are the conditions/situations of practice varied?",
                            answer: ""
                        },
                        {
                            text: "Do the learning conditions replicate on-the-job conditions?",
                            answer: ""
                        },
                        {
                            text: "Are experts at the task (role models, mentors) employed to enhance learning?",
                            answer: ""
                        }
                    ]
                },
                {
                    number: 7,
                    questions: [
                        {
                            text: "Do leaders learn from feedback and reflection?",
                            answer: ""
                        },
                        {
                            text: "Does the learning draw on the study of previous leaders/historical examples?",
                            answer: ""
                        },
                        {
                            text: "Does the learning include a test of the leader’s level of learning?",
                            answer: ""
                        },
                        {
                            text: "Does the test employed fully and accurately assess leader learning?",
                            answer: ""
                        }
                    ]
                }

            ],
            current_page: 1,
            mentee: {},
            title: ""

        },

    leverageRoleModels: {

            template_type : 'LEVERAGE_ROLE_MODELS'.toLowerCase(),
            pages: [
                {
                    number: 1,
                    template_name: "",
                    date: ""
                },
                {
                    number: 2
                },
                {
                    number: 3,
                    ways: [
                        {
                            mentee:{},
                            to_Leverage: "",
                            to_provide: ""
                        },
                        {
                            mentee:{},
                            to_Leverage: "",
                            to_provide: ""
                        },
                        {
                            mentee:{},
                            to_Leverage: "",
                            to_provide: ""
                        },
                        {
                            mentee:{},
                            to_Leverage: "",
                            to_provide: ""
                        }
                    ]
                }

            ],
            current_page: 1,
            mentee: {},
            title: ""

        },
        trainingAndProfessionalDevelopment: {

            template_type : 'TRAINING_AND_PROFESSIONAL_DEVELOPMENT'.toLowerCase(),
            pages: [
                {
                    number: 1,
                    template_name: "",
                    date: ""
                },
                {
                    number: 2
                },
                {
                    number: 3,
                    applications: [
                        {
                            title: "Leader Mission Essential Task List Training",
                            purpose: "",
                            method: ""

                        },
                        {
                            title: "Mandatory Army Training Requirements",
                            purpose: "",
                            method: ""

                        },
                        {
                            title: "Build Leader Cohesion/ Esprit de Corps",
                            purpose: "",
                            method: ""

                        }

                    ]
                },
                {
                    number: 4,
                    applications: [
                        {
                            title: "Issue Command Guidance",
                            purpose: "",
                            method: ""

                        },
                        {
                            title: "Leader Career Path Education",
                            purpose: "",
                            method: ""

                        }

                    ]
                }

            ],
            current_page: 1,
            mentee: {},
            title: ""

        },
        soarObservationSource: {

            template_type : 'SOAR_OBSERVATION_SOURCE'.toLowerCase(),
            pages: [
                {
                    number: 1,
                    template_name: "",
                    date: ""
                },
                {
                    number: 2
                },
                {
                    number: 3,
                    feedbacks: [
                        {
                            text: "Describe the situation/conditions of the assessment.",
                            title: "Situation",
                            answer: ""

                        },
                        {
                            text: "Describe behavior and impact on mission and/or Soldiers.",
                            title: "Observation",
                            answer: ""

                        }

                    ]
                },
                {
                    number: 4,
                    feedbacks: [
                        {
                            text: "Identify FM 6-22 competency (see page 19); assess proficiency.",
                            title: "Associate & Assess",
                            answer: ""

                        },
                        {
                            text: "Note appropriate feedback, praise, or correction. Recommended action to sustain/improve leader behavior.",
                            title: "Reinforce & Recommend",
                            answer: ""

                        }

                    ]
                }

            ],
            current_page: 1,
            mentee: {},
            title: ""

        },
        studyTool: {

            template_type : 'STUDY_TOOL'.toLowerCase(),
            pages: [
                {
                    number: 1,
                    template_name: "",
                    date: ""
                },
                {
                    number: 2
                },
                {
                    number: 3,
                    questions: [
                        {
                            text: "Book/Article/Reference",
                            answer: ""
                        },
                        {
                            text: "Name of Leader",
                            answer: ""
                        },
                        {
                            text: "Position",
                            answer: ""
                        }
                    ]
                },
                {
                    number: 4,
                    questions: [
                        {
                            text: "Describe the leader’s environment/situation.",
                            answer: ""
                        },
                        {
                            text: "Who was the leader leading?",
                            answer: ""
                        },
                        {
                            text: "How did the leader attempt to influence the situation/ people? (Use FM 6-22 Leadership Re- quirements Model language)",
                            answer: ""
                        }
                    ]
                },
                {
                    number: 5,
                    questions: [
                        {
                            text: "What were the positive and negative outcomes?",
                            answer: ""
                        },
                        {
                            text: "What were the leader’s strengths and development needs?",
                            answer: ""
                        },
                        {
                            text: "What lessons from this leader’s experience can we apply immediately and/or down the road?",
                            answer: ""
                        }
                    ]
                }

            ],
            current_page: 1,
            mentee: {},
            title: ""

        },
        tigerTeamImplementationTool: {

            template_type : 'TIGER_TEAM_IMPLEMENTATION_TOOL'.toLowerCase(),
            pages: [
                {
                    number: 1,
                    template_name: "",
                    date: ""
                },
                {
                    number: 2
                },
                {
                    number: 3,
                    questions: [
                        {
                            text: "ID problem/opportunity with critical mission/ Soldier impact",
                            answer: ""
                        },
                        {
                            text: "Brief chain of command on Tiger Team authority and process",
                            answer: ""
                        },
                        {
                            text: "Select cross-functional team based on expertise, innovative thinking, and self-motivation",
                            answer: ""
                        }
                    ]
                },
                {
                    number: 4,
                    questions: [
                        {
                            text: "Identify Tiger Team facilitator(s); clarify their roles/responsibilities",
                            answer: ""
                        },
                        {
                            text: "Isolate/protect Team from operational responsibilities",
                            answer: ""
                        },
                        {
                            text: "Provide Team with reachback capability/ access to knowledge",
                            answer: ""
                        }
                    ]
                },
                {
                    number: 5,
                    questions: [
                        {
                            text: "Authorize direct coordination with higher/adjacent experts",
                            answer: ""
                        },
                        {
                            text: "Establish quick deadlines for results",
                            answer: ""
                        },
                        {
                            text: "Report directly to commander with solution",
                            answer: ""
                        }
                    ]
                },
                {
                    number: 6,
                    questions: [
                        {
                            text: "Facilitate solution implementation",
                            answer: ""
                        },
                        {
                            text: "AAR Tiger Team process",
                            answer: ""
                        },
                        {
                            text: "Announce solutions, recognize Tiger Team members and supporting personnel. Officially disband.",
                            answer: ""
                        }
                    ]
                }

            ],
            current_page: 1,
            mentee: {},
            title: ""

        },
        unitLeaderDevelopmentRolesAndResponsibilities: {

            template_type : 'UNIT_LEADER_DEVELOPMENT_ROLES_AND_RESPONSIBILITIES'.toLowerCase(),
            pages: [
                {
                    number: 1,
                    template_name: "",
                    date: ""
                },
                {
                    number: 2
                },
                {
                    number: 3,
                    positions: [
                        {
                            title:"",
                            roles_responsibilities: ""
                        },
                        {
                            title:"",
                            roles_responsibilities: ""
                        },
                        {
                            title:"",
                            roles_responsibilities: ""
                        }
                    ]
                }

            ],
            current_page: 1,
            mentee: {},
            title: ""

        }
  }
}]);