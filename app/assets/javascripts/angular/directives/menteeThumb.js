rowan.resourceTemplate.directive('menteeThumb', ['$compile', '$http', '$templateCache', function($compile, $http, $templateCache) {
    return {
        restrict: 'E',
        scope : {
            mentee : '='
        },
        replace: true,
        template: '<div class="profile-header"><img src="{{mentee.image}}" ><span>{{mentee.title}}</span> </div>'
    }
}]);