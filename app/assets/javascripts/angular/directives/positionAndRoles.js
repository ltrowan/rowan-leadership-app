rowan.resourceTemplate.directive('positionAndRoles', ['$compile', '$http', '$templateCache', function($compile, $http, $templateCache) {
    return {
        restrict: 'E',
        scope : {
            positions : '=',
            button: "="
        },
        replace: true,
        templateUrl: 'common/position_and_roles.html',
        controller: ['$scope',function ($scope) {
            $scope.newPosition = function newPosition() {
                $scope.positions.push( {
                    title:"",
                    roles_responsibilities: ""
                });
            };
        }]
    }
}]);