rowan.resourceTemplate.directive('droppableCommunity', ['$compile', '$http', '$templateCache', function($compile, $http, $templateCache) {
    return {
        templateUrl: 'common/droppable_community.html',
        restrict: 'E',
        replace: true,
        link : function(scope, element, attrs){
          if(scope.community.is_owner){
            $(element).droppable({
              tolerance: "pointer",
              drop : function(event,ui){
                ele = angular.element(ui.draggable)
                contact_data = ele.scope().contact
                community_contacts = scope.community.contacts;
                var exists = false;
                for(var i=0; i< community_contacts.length; i++){
                  if(community_contacts[i].id==contact_data.id){
                    exists = true;
                    break;
                  }
                }
                if(!exists){
                  scope.addCommunityContact(scope.community,contact_data);
                  // scope.$apply(function(){
                  //   contact_data.new_record = true;
                  //   community_contacts.push(contact_data);
                  // });                  
                }
              }
            });
          }
        }
    };
}]);