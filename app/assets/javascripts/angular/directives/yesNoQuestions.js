
rowan.resourceTemplate.directive('yesNoQuestions', ['$compile', '$http', '$templateCache', function($compile, $http, $templateCache) {
  return {
    restrict: 'E',
    scope : {
      questionsData : '=',
      headerText : '@'
    },
    replace: true,
    templateUrl: 'common/yes_no_question.html'
  }
}]);