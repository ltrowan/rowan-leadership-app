rowan.resourceTemplate.directive('employRoleModels', ['$compile', '$http', '$templateCache', function($compile, $http, $templateCache) {
    return {
        restrict: 'E',
        scope : {
            examples : '='
        },
        replace: true,
        templateUrl: 'common/employ_role_models.html',
        controller: ['$scope',function ($scope) {
            $scope.newExample = function newExample() {
                $scope.examples.push( {
                    mentee:{},
                    to_Leverage: "",
                    to_provide: ""
                });
            };


        }]
    }
}]);