rowan.resourceTemplate.directive('statusTextComboQuestions', ['$compile', '$http', '$templateCache', function($compile, $http, $templateCache) {
    return {
        restrict: 'E',
        scope : {
            questionsData : '=',
            pageNumber : '=',
            headerText : '@'
        },
        replace: true,
        templateUrl: 'common/status_text_combo_question.html'
    }
}]);