rowan.resourceTemplate.directive('draggableContact', ['$compile', '$http', '$templateCache', function($compile, $http, $templateCache) {
    return {
        templateUrl: 'common/draggable_contact.html',
        restrict: 'E',
        replace: true,
        scope :{
            contactData : '='
        },
        link : function(scope, element, attrs){
            $(element).draggable({
             revert: true, 
             helper: "clone"
         });
        }
    };
}]);