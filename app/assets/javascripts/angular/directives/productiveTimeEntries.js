rowan.resourceTemplate.directive('productiveTimeEntries', ['$compile', '$http', '$templateCache', function($compile, $http, $templateCache) {
    return {
        restrict: 'E',
        scope : {
            productiveTimes : '='
        },
        replace: true,
        templateUrl: 'common/productive_time_entries.html',
        controller: ['$scope',function ($scope) {
            $scope.newProductiveTime = function newProductiveTime() {
                $scope.productiveTimes.push( {
                    date: "",
                    observation_window: "",
                    leader: {},
                    situation_to_observe: ""
                });
            };

            $scope.resize = function($event){
                var ele = $event.target;
                var offset = ele.offsetHeight - ele.clientHeight;
                $(ele).css('height', 'auto').css('height', ele.scrollHeight + offset);

            };
        }]
    }
}]);