rowan.resourceTemplate.directive('preview', ['$compile', '$http', '$templateCache', function($compile, $http, $templateCache) {
    return {
        template: '<div ng-include="getPageUrl()"/>',
        restrict: 'E',
        replace: true,
        controller: ['$scope', function($scope) {
            //function used on the ng-include to resolve the template
            $scope.getPageUrl = function() {
                return $scope.template_data.template_type+'/preview.html';
            };
        }]
    };
}]);