rowan.resourceTemplate.directive('yesNoSwitch', ['$compile', function($compile) {
  var linker = function (scope, element, attrs) {
  	switch(scope.status){
  		case 1:
  		$(element).find("span.no").hide();
  		$(element).find("span.yes").show();
  		$(element).find("span.yes").addClass('green-font');
  		break;
  		case 2:
  		$(element).find("span.yes").hide();
  		$(element).find("span.no").show();
  		$(element).find("span.no").addClass('red-font');
  		break;
  	}
  	$(element).on('click', 'span', function(event) {
    if($(this).hasClass('green-font') || $(this).hasClass('red-font')) {
      $(this).removeClass('green-font');
      $(this).removeClass('red-font');
      $(this).siblings('span').show();
      scope.status = 0;
    }
    else{
      $(this).siblings('span').hide();
      var thisText = $(this).text();
      if(thisText == 'Yes'){
        $(this).addClass('green-font');
        scope.status = 1;
      }
      else{
        $(this).addClass('red-font');
        scope.status = 2;
      }
    }
  });
  };
  return {
    restrict: 'E',
    scope : {
      status : '='
    },
    replace: true,
    template: '<div class="yesNo"><span class="table-btn yes" data-type="yes" style="display: inline;">Yes</span> <span class="table-btn no" data-type="no" style="display: inline;">No</span></div>',
    link : linker
  }
}]);