rowan.resourceTemplate.directive('responsibilities', ['$compile', '$http', '$templateCache', function($compile, $http, $templateCache) {
    return {
        restrict: 'E',
        scope : {
            responsibilities : '=',
            button: "="
        },
        replace: true,
        templateUrl: 'common/responsibilities.html',
        controller: ['$scope',function ($scope) {
            $scope.newResponsibility = function newResponsibility() {
                $scope.responsibilities.push( {
                    text: "",
                    readiness: ""
                });
            };
        }]
    }
}]);