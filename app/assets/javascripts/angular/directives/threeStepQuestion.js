rowan.resourceTemplate.directive('threeStepQuestions', ['$compile', '$http', '$templateCache', function($compile, $http, $templateCache) {
  return {
    restrict: 'E',
    scope : {
      questionsData : '=',
      addQuestion : '=',
      pageNumber : '='
    },
    replace: true,
    templateUrl: 'common/three_step_questions.html',
    controller: ['$scope',function ($scope) {
            $scope.newQuestion = function newQuestion() {
              $scope.questionsData.push( {
                    title: "Additional Assignment Demands:",
                    text: "",
                    is_part_of_job: false,
                    status: 0,
                    answer: "",
                    custom_question: true
                });
            };
        }]
  }
}]);