rowan.resourceTemplate.controller('UserCommunityCtrl', ['$scope',
    '$rootScope',
    '$sce',
    '$q',
    'userService',
    'communityDataService',
    '$window',
    function($scope, $rootScope, $sce, $q, userService, communityDataService, $window){
      $scope.user_communities = [];
      $scope.user_contacts = [];
      $scope.new_community = {};
      $scope.show_contacts = true;
      $scope.current_user = {};
      $scope.countries = [];
      $scope.search_contact_keyword = "";
      $scope.searchedUser = [];
      $scope.init = function(){
        $scope.user_communities = $.parseJSON(communityDataService.getCommunitiesData());
        $scope.user_contacts = $.parseJSON(communityDataService.getUserContacts());
        $scope.countries = $.parseJSON(communityDataService.getCountryData());
        $scope.ranks = $.parseJSON(communityDataService.getRankData());
        $scope.current_user = $.parseJSON(communityDataService.currentUser());
        $scope.redirectBackTo = communityDataService.getRedirectBackTo();
        $scope.selected_country = {id:0};
        $scope.selected_state = {id:0};
        $scope.selected_base = {id:0};
        $scope.selected_rank = {id:0};
        $scope.new_community.show_setting = false;
      };

    $scope.save_community = function(community){
      userCommunitySavePromise = userService.saveUserCommunity(community);
        $q.all([userCommunitySavePromise]).then(function(data){
        if(data[0].success){
          if(!community["id"]){ //new community added
             if(community["owner"] && data[0].community.owner.id!=community["owner"]["originalObject"]["id"])
               data[0].community.is_owner=false;

             $scope.user_communities.push(data[0].community);
          }else{
            community.contacts = JSON.parse(JSON.stringify(data[0].community.contacts));
            community.owner = data[0].community.owner;
            community.is_owner = data[0].community.is_owner;
            community.show_box = false;
          }   
          $scope.new_community = {};      
          $scope.new_community.show_setting = false;
        }       	
      });
    };

    $scope.save_allCommunity = function(community){
      /* Save button for all communities need not  
      * save communities and its contact again as everything is being saved 
      * independently
      own_communities = $.grep($scope.user_communities,function(com){
            return com.is_owner;
      });
      if(own_communities.length>0){
        userCommunitySavePromise = userService.saveAllCommunity(own_communities);
        $q.all([userCommunitySavePromise]).then(function(data){
        if(!data[0].success){
           $window.alert("Error in save! Try again!");
        }else{
          $window.location.href = $scope.redirectBackTo;
        }         
      });
      }else{
          $window.location.href = $scope.redirectBackTo;
      }
      */
      $window.location.href = $scope.redirectBackTo;
    };

    var pendingTask;
    function fetch(){
      if($scope.search_contact_keyword.length >2){
         userService.searchContacts($scope.search_contact_keyword, 
         $scope.selected_country.id, 
         $scope.selected_state.id,
         $scope.selected_base.id,
         $scope.selected_rank.id).then(function(response){
            $scope.searchedUser = response;
        });
      }
    };
    
 
    $scope.searchContacts = function(){
      if(pendingTask) {
        clearTimeout(pendingTask);
      }
      pendingTask = setTimeout(fetch, 800);
    };

    $scope.setCountry = function(country){
      $scope.selected_country = country;
    };
    $scope.setState = function(state){
      $scope.selected_state = state;
    }
    $scope.setBase = function(base){
      $scope.selected_base = base;
    }
    $scope.setRank = function(rank){
      $scope.selected_rank = rank;
    }

    $scope.addContacts = function(){
      selected_users_ids = [];
      selected_users = []
      for(i = 0; i < $scope.searchedUser.length; i++){
        if($scope.searchedUser[i].selected && !$scope.alreadyinContact($scope.searchedUser[i].id)){
          selected_users_ids.push($scope.searchedUser[i].id);
          selected_users.push($scope.searchedUser[i]);
        }
      }
      if(selected_users_ids.length >0){
        userService.addContacts($scope.current_user.id,selected_users_ids).then(function(response){
        if(response.success){
          $.each(selected_users,function(index,usr){
            $scope.user_contacts.push(usr);
          });
          $scope.show_contacts = true;          
        }else{
          $window.alert('Error in adding contact! Try Again!');
        }

      });
      }
    };

    $scope.deleteCommunity = function(community_id,index){
      userService.deleteUserCommunity(community_id).then(function(response){
        if(response.success){
          $scope.user_communities.splice(index,1);
        }else{
          $window.alert('Error in deleting Community! Try Again!');
        }

      });

    };
    $scope.deleteCommunityContact = function(community,contact,community_user_index){
      userService.deleteCommunityContact(community.id,contact.id).then(function(response){
        if(response.success){
          community.contacts.splice(community_user_index,1);
        }else{
          $window.alert('Error in deleting Community! Try Again!');
        }
      });
    };
    $scope.addCommunityContact = function(community,contact){
      userService.addCommunityContact(community.id,contact.id).then(function(response){
        if(response.success){
          community.contacts.push(contact);
        }else{
          $window.alert('Error in deleting Community! Try Again!');
        }
      });
    };
    $scope.alreadyinContact = function(contact_id){
        var exists = false;
        for(var i=0;i < $scope.user_contacts.length; i++){
          if($scope.user_contacts[i].id == contact_id){
             exists = true;
             break;
          }
        }
        return exists;
    }

}]);