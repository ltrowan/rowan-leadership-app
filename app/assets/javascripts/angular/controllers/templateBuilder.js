rowan.resourceTemplate.controller('TemplateCtrl', ['$scope',
    '$rootScope',
    '$sce',
    '$q',
    'masterDataFactory',
    'templateDataService',
    'userService',
    '$window',
    '$http',
    '$filter',
    function($scope, $rootScope, $sce, $q, masterDataFactory, templateDataService, 
      userService,$window,$http, $filter){
      $scope.user_template_id = 0;
      $scope.userContacts = [];
      $scope.multiselect = {}
      $scope.multiselect.selectedContacts = [];
      $scope.is_review_template = false;
      $scope.init = function(){
        $scope.redirectBackTo = templateDataService.getRedirectBackTo();
      	$scope.template_type = templateDataService.getTemplateType();
	      template_object = templateDataService.getTemplateData();
	      if(template_object.length==0){
	        $scope.template_data = masterDataFactory.template_definition($scope.template_type);
	      }else{
	      	$scope.user_template_id = parseInt(templateDataService.getUserTemplateId());
	        $scope.template_data = $.parseJSON(template_object);
	      }
        $scope.is_review_template = templateDataService.get_review_template_flag();        
      };

      $scope.save_template = function(){
       old_page_number = $scope.template_data.current_page;
       if($scope.template_data.current_page < $scope.template_data.pages.length ){
            $scope.template_data.current_page +=1;            
          }else{
            $scope.template_data.current_page = 'preview';
       } 
       templateSavePromise = userService.saveUserTemplate($scope.template_type,$scope.template_data,$scope.user_template_id);
       $q.all([templateSavePromise]).then(function(data){
        if(data[0].success){
          $scope.user_template_id = data[0].user_template_id;
        }else{
          $scope.template_data.current_page = old_page_number;
        }
       	
      });
    };

    $scope.save_complete = function(){
        $scope.template_data.current_page = 'preview';
    	templateSavePromise = userService.saveUserTemplate($scope.template_type,$scope.template_data,$scope.user_template_id);
       $q.all([templateSavePromise]).then(function(data){
        if(data[0].success){
          $scope.user_template_id = data[0].user_template_id;
//          $window.location.href = '/templates/'+ $scope.template_type.replace(/_/g,"-").toLowerCase();
        }        
      });    	

    };
    $scope.collectQuestions = function(pages, onwards){
        var flatten = [];
        angular.forEach(pages, function(page){
            if(page.number >= onwards){
                var questions = page.questions;
                if(questions){
                    angular.forEach(questions, function(question){
                        flatten.push(question);
                    });
                }
            }
        });
        return flatten;

    };

  $scope.refreshContacts = function(contact) {
    if(contact.length>1){
      var params = {query: contact};
      return $http.get('/users/contacts.json', {params: params})
        .then(function(response) {
          $scope.userContacts = $filter('selectedContactFilter')(response.data.results,$scope.multiselect.selectedContacts);
        });
    }
  };

$scope.sendTemplateLink = function(){
  if($scope.multiselect.selectedContacts.length > 0){
    templateSendPromise = userService.sendTemplateToContacts($scope.user_template_id,$scope.multiselect.selectedContacts);
       $q.all([templateSendPromise]).then(function(data){
        if(data[0].success){
           $window.location.href = $scope.redirectBackTo; 
        }else{
          alert('Error while sending template, Please Try Again!!');
        }
        
    });
   }
}
}]);