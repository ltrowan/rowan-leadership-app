 var url= "http://" + window.location.host +"/tabs/";
 var parent_anchor = "";
 function update_url(anchor_obj,url_class){
    var anchor_text = $(anchor_obj).val();
    var anchor_tab_obj = $("#tab_anchor");

    if (anchor_tab_obj.length>0)
 	   parent_anchor = anchor_tab_obj.val();
 	
    anchor_text = anchor_text.replace(/\s/g, "");
    $(anchor_obj).val(anchor_text);
    if (anchor_text.length>0){
 		anchor_text = parent_anchor.length>0 ? "#"+anchor_text : anchor_text;
 	} 
    $("."+url_class).text(url+parent_anchor+anchor_text);
 }

 function bind_url(){
 	var anchor_text = $(".anchor_box").val(); 
 	var anchor_tab_obj = $("#tab_anchor");

 	if (anchor_tab_obj.length>0)
 	   parent_anchor = anchor_tab_obj.val();

 	if (anchor_text!=null && anchor_text.length>0){
 		anchor_text = parent_anchor.length>0 ? "#"+anchor_text : anchor_text;
 	}else{
 		anchor_text="";
 	} 	
 	$(".url_value").text(url+parent_anchor+anchor_text);
 }
 $(document).ready(function(){ 	
 	setTimeout(function(){
 		bind_url();
 	},1000);
 });