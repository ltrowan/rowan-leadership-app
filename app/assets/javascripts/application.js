// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require jquery-ui
//= require turbolinks
//= require bootstrap-sprockets
//= require scrollReveal
//= require sortable.min
//= require assessments
//= require jquery.nstSlider
//= require jquery.remotipart
//= require jquery.Jcrop
//= require dropzone
//= require bootstrap-multiselect
//= require templates-common
//= require mediaelement_rails
//= require tags

var VideoPlayer;

VideoPlayer = function(){
    function VideoPlayer(){}

    VideoPlayer.init = function(){
        VideoPlayer.initDimensions();
    };

    VideoPlayer.initDimensions = function(){
        $("video").each(function(){
            var ele = $(this);

            ele.on('canplay', function(){
                ele.prop('height', ele.css("height").replace("px", ""));
                ele.prop('width', ele.css("width").replace("px", ""));
                ele.mediaelementplayer();
            });
//
        });
    };
    return VideoPlayer;
}();

$.fn.inView = function(inViewType){
    var viewport = {};
    viewport.top = $(window).scrollTop();
    viewport.bottom = viewport.top + $(window).height();
    var bounds = {};
    bounds.top = this.offset().top;
    bounds.bottom = bounds.top + (this.outerHeight()/2);
    switch(inViewType){
        case 'bottomOnly':
            return ((bounds.bottom <= viewport.bottom) && (bounds.bottom >= viewport.top));
        case 'topOnly':
            return ((bounds.top <= viewport.bottom) && (bounds.top >= viewport.top));
        case 'both':
            return ((bounds.top >= viewport.top) && (bounds.bottom <= viewport.bottom));
        default:
            return ((bounds.top >= viewport.top) && (bounds.bottom <= viewport.bottom));
    }
};

var App = App || {};

App.ErrorHandler = {
  add_errors: function(container_id, error_hash){
    $el = $(container_id);
    this.clear_errors(container_id);
    errors = JSON.parse(error_hash)
    $.each(errors, function(key,errors_arr){
      $input = $el.find("[data-errorkey="+key+"]")
      $input.closest(".form-group").addClass("error").append("<span class='error text-danger'>"+errors_arr.join(", ")+"</span>")
    });
  },

  clear_errors: function(container_id){
    $(container_id).find("span.error").each(function(){
      $(this).closest(".form-group").removeClass("error");
      $(this).remove();
    });
  }
}


App.DrawerWindow = {
  init: function(){
    self = this;
    $(document).on('click', '.open-left-drawer', function(){
      if($("#drawer-window-left").hasClass("closed"))
        self.show_left_drawer();
      else
         self.hide_left_drawer();
      if (navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1)
        self.adjust_quicknav_bkgnd();
    });
    $(document).on('click', '.open-right-drawer .fa-users', function(){
      if($("#drawer-window-right").hasClass("closed"))
        self.show_right_drawer();
      else 
        self.hide_right_drawer();
            
      if (navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1)
        self.adjust_quicknav_bkgnd();
    });

  },
  show_left_drawer: function(){
    self.hide_right_drawer();
    $(".open-left-drawer").addClass("open");
    $("#drawer-window-left").removeClass("closed");
    $("#pageWrapper").addClass("left-drawer-opened");
  },
  hide_left_drawer: function(){
    $(".open-left-drawer").removeClass("open");
    $("#drawer-window-left").addClass("closed");
    $("#drawer-window-left .sub-menu").fadeOut();
    $("#pageWrapper").removeClass("left-drawer-opened");
  },
  show_right_drawer: function(){
    self.hide_left_drawer();
    $("#drawer-window-right").removeClass("closed");
    $("#pageWrapper").addClass("right-drawer-opened");
  },
  hide_right_drawer: function(){
    $("#drawer-window-right").addClass("closed");
    $("#pageWrapper").removeClass("right-drawer-opened");
  },
  adjust_quicknav_bkgnd: function(){
    //setTimeout("$('#quick_navigation').css('width', '110%')", 250);
    //setTimeout("$('#quick_navigation').css('width', '100%')", 500);
    $("#pageWrapper").css('background', 'rgba(80,83,87,0.8)');
  }
};


$(function() {
  App.DrawerWindow.init();
  VideoPlayer.init()
  
  window.scrollReveal = new scrollReveal();
  
  $(document).on('click', '.closeTTP', function(e){
    $(e.target).closest(".ttp-description.collapse.in").collapse('hide');
  });
  
  $(document).on('click', '.replay-music', function(e){
    var temp = $(e.target).closest(".ttp-description").siblings('audio');
    var music = document.getElementById(temp.attr('id'));
    music.currentTime = 0;
  });
  

  
  window.nextStep = function(){
    $currentStep = $('.pageStepView.active');
    $nextStep = $currentStep.next('.pageStepView');
    $currentStep.addClass("slideUp").removeClass('active');
    $nextStep.addClass('active');
  }
  
  $(document).on('click', '.slideNext', function(e){
    window.nextStep();
  });

  $(document).on('click', 'a.chevron[data-toggle="collapse"]', function(e){
    var $el = $(this).find('i');
    if($el.hasClass('fa-chevron-down')){
      $el.removeClass("fa-chevron-down").addClass("fa-chevron-up");
    }else{
      $el.removeClass("fa-chevron-up").addClass("fa-chevron-down");
    }
  });
  
  $(document).on('click', '.expandScoreCard', function(){
    $(this).closest('.assessment-section').addClass('expanded').siblings('.assessment-section').hide();
  });
  $(document).on('click', '.collapseScoreCard', function(){
    $(this).closest('.assessment-section').removeClass('expanded').siblings('.assessment-section').show();
  });
  
  // $('#submitMilEmail').click(function(){
  //   var email = $('#dummy_military_email').val();
  //   if(email.substr(email.length - 4) == ".mil"){
  //     $('#miltary_email').val(email);
  //     window.nextStep();
  //   }else{
  //     $(".mil-email-error").html("Please enter an email that ends with .mil");
  //   }
  // });
    $(document).on('click', function(event){
        var ele = $(event.target);
        if(!(ele.is(".init-delete span") || ele.is(".init-delete") || ele.is(".init-delete .fa-remove")) ){
            $(".init-delete").show();
            $(".confirm-delete").hide();
        }
    });
    $(document).on('click', function(event){
        var ele = $(event.target);
        if(!(ele.is(".community-init-delete span") || ele.is(".community-delete")) ){
            $(".community-delete").show();
            $(".community-confirm-delete").hide();
        }
    });

    var closeLeftDrawerOnClose;
    closeLeftDrawerOnClose = (function() {
        function closeLeftDrawerOnClose() {}
        var self = closeLeftDrawerOnClose;

        self.init = function(){
            self.setPreviousScrollTop();
            self.onScroll();
            if (navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1)
              self.onScrollExpandQuickNav();
        };

        self.onScroll = function(){
            $(document).scroll(function() {
                var totalScroll = Math.abs($("#previous_scroll_top").val()-$(document).scrollTop());

                if(totalScroll > 100){
                    App.DrawerWindow.hide_left_drawer();
                    self.setPreviousScrollTop();
                }
                if (navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1)
                  self.onScrollExpandQuickNav();
            });
            
        };
        
        self.setPreviousScrollTop = function(){
            $("#previous_scroll_top").val($(document).scrollTop())
        };
        
        self.onScrollExpandQuickNav = function(){
          //setTimeout("$('#quick_navigation').css('width', '120%')", 250);
          //setTimeout("$('#quick_navigation').css('width', '100%')", 500);  
          $("#pageWrapper").css('background', 'rgba(80,83,87,0.8)');
        };

        setTimeout(self.init, 1000);
        
        return closeLeftDrawerOnClose;

    })();
});

$(function() {
  return $(document).on('change', '#user_country_id', function(evt) {
    return $.ajax('/update_states', {
      type: 'GET',
      dataType: 'script',
      data: {
        country_id: $("#user_country_id option:selected").val()
      },
      error: function(jqXHR, textStatus, errorThrown) {
        return console.log("AJAX Error: " + textStatus);
      },
      success: function(data, textStatus, jqXHR) {
        return console.log("Dynamic country select OK!");
      }
    });
  });
});

$(function() {
  return $(document).on('change', '#user_state_id', function(evt) {
    return $.ajax('/update_bases', {
      type: 'GET',
      dataType: 'script',
      data: {
        state_id: $("#user_state_id option:selected").val()
      },
      error: function(jqXHR, textStatus, errorThrown) {
        return console.log("AJAX Error: " + textStatus);
      },
      success: function(data, textStatus, jqXHR) {
        return console.log("Dynamic state select OK!");
      }
    });
  });
});
