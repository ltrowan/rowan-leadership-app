$(function() {
  $( "#datepicker" ).datepicker();
  $('body').on('focus',".datepicker", function() {
      $(this).datepicker();
  });
  $('body').on('focus',".timepicker", function() {
      $(this).timepicker();
  });

});

$(function () {
  $('[data-toggle="tooltip"]').tooltip()
});

$(function() {
  var availableTags = [
    "Aaron",
    "Adam",
    "Baker",
    "Bane",
    "Chris",
    "Charlie",
    "Dave",
    "Damien",
    "Elliot",
    "Erlang",
    "Fortran",
    "Gram",
    "Hacker"
  ];
  $( "#template-tags" ).autocomplete({
    source: availableTags
  });
});
