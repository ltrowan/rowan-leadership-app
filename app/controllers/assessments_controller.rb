class AssessmentsController < ApplicationController

  before_action :set_redirect_back_to
  
  def show
    @assessment = Assessment.find_by_id(params[:id])
    @questions = @assessment.questions if @assessment
    session[:assessment_params] ||= {}
    session[:assessment_params]["answers"] ||= {}
    redirect_to root_path unless @assessment
  end
  
  def save
    @assessment = Assessment.find_by_id(params[:assessment_id])
    if params[:commit] == "get results"
      session[:assessment_params] = params[:answers]
      sum = 0;
      session[:assessment_params].each{|k, v| sum += v["value"].to_i}
      @score_range = @assessment.score_ranges.where("start <= ?", sum).where("ending >= ?", sum).first
      session.delete(:assessment_params)
    else
      session[:assessment_params] = params[:answers]
    end
  end
  
end
