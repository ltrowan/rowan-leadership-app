class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception 
  before_filter :authenticate_user!

  def set_submenu
  	@main_menu_tabs = Tab.all.order(position: :desc).select(:id,:title,:position)
  end
  	
  def countries_nested_data
  	  Country.all.map{|c| {
  	  	id:c.id,
  	  	name:c.name,
  	  	states:c.states.map{|s| {
  	  		id:s.id,
  	  		name:s.name,
  	  		bases:s.base_locations.map{|b| {id:b.id,name:b.name}}}}}}
  end	

  def user_hash user
    image_url = user.image.exists? ? user.image(:thumb) : 'assets/'+user.image(:thumb) 
    {id: user.id, 
      first_name: user.first_name, 
      last_name: user.last_name, 
      email: user.email,
      image_url: image_url,
      rank_name: user.rank.name,
      country_name: user.country.name,
      state_name: user.state.name,
      base_location: user.base_location.name} if user.present?
  end

  private
  def set_redirect_back_to
    @redirect_back_to = session[:tab_last_visited] || root_path
  end

  def init_contacts_and_communities
    @contacts_and_communities = {}
    @contacts_and_communities[:communities] = current_user.communities
    @contacts_and_communities[:contacts] = current_user.contacts
  end

end
