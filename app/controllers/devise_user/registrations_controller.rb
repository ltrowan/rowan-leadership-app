class DeviseUser::RegistrationsController < Devise::RegistrationsController
  before_filter :configure_permitted_parameters
  before_filter :authenticate, :only => [:add_image, :save_image]
  respond_to :html, :js, :json
  include UserFormData
  
  def new
    super
    load_form_data
  end
  
  def create
    build_resource(sign_up_params)
    resource.confirmation_token = SecureRandom.base64(24)
    resource.save
    yield resource if block_given?
    if resource.persisted?
      set_flash_message :notice, :signed_up if is_flashing_format?
      sign_up(resource_name, resource)
      begin
        #UserMailer.welcome_email(resource).deliver_later
      end
      respond_to do |format|
        format.json { render json: { success: true } }
        format.js {}
        format.html {redirect_to after_sign_up_path_for(resource)}      
      end
    else
      clean_up_passwords resource
      respond_to do |format|
        format.json { render json: {success: false, errors: resource.errors} }
        format.js {}
        format.html {render 'devise/registrations/new'}      
      end
    end
  end

  def edit
    load_form_data
    render :edit
  end
  
  def add_image
    authenticate  
  end
  
  def save_image
    @user = User.find(params[:id])
    if @user.update(image_params)
      if @user.cropping?
        @user.reprocess_image
        @cropped=true
      end
    else
      render "add_image"
    end
  end
  
  def update_states
    load_states
    respond_to do |format|
      format.js
    end
  end

  def update_bases
    load_bases
    respond_to do |format|
      format.js
    end
  end
  
  protected

  def configure_permitted_parameters
    parameters = [
        :first_name,:last_name, :rank_id, :base_location_id, :country_id, :state_id, :miltary_email, :confirmation_token
    ]
    devise_parameter_sanitizer.for(:sign_up).push(parameters)
    devise_parameter_sanitizer.for(:account_update).push(parameters)
  end

  def image_params
    params.require(:user).permit(:image, :crop_x, :crop_y, :crop_w, :crop_h)
  end
  
  def after_sign_up_path_for(resource)
    "/users/#{resource.id}/add_image"
  end
  
  def authenticate
    if current_user
      @user = current_user
    else
      redirect_to new_user_session_path
    end
  end
end