class DeviseUser::SessionsController < Devise::SessionsController
  include UserFormData

  def new
    self.resource = resource_class.new(sign_in_params)
    load_form_data
    clean_up_passwords(resource)
    yield resource if block_given?
    respond_with(resource, serialize_options(resource))
  end

end