class TagsController < ApplicationController


  def index
    @tags = ActsAsTaggableOn::Tag.includes(taggings: :taggable)
  end

end
