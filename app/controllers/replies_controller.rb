class RepliesController < ApplicationController
  def create
    @content_block_post = ContentBlockPost.find(params[:content_block_post_id])
    if /reply_[0-9]/.match(params[:reply][:content])
      @reply = @content_block_post.replies.where("id = ?", params[:reply][:content].split("_").last).last
    else
      @reply = @content_block_post.replies.build(reply_params)
      @reply.user = current_user
      @reply.save
      validate_video_to_be_deleted
    end
  end

  def new
    @content_block_post = ContentBlockPost.find(params[:content_block_post_id])
    init_contacts_and_communities
  end

  def destroy
    @reply = Reply.find(params[:id]).destroy
  end

  private
  def reply_params
    params.require(:reply).permit(:content)
  end

  def validate_video_to_be_deleted
    if !params[:reply_video_link].nil?
      @content_block_post.replies.where("id = ?", params[:reply_video_link].split("_").last).last.destroy rescue nil
    end
  end
end
