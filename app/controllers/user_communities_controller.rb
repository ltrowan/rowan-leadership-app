class UserCommunitiesController < ApplicationController
  skip_before_filter :verify_authenticity_token

	def save_community
			community = params[:community]
		success, comm = save_user_community community
		if success 
		   new_comm = get_user_community comm
		end	

		respond_to do |format|
		  format.json{render json: {:success=>success,:community=>new_comm}}
		end
	end	 

	def show
	  communities_data = get_user_communities
	  @communities_data = communities_data.to_json
	  user_contacts =  users_hash current_user.contacts
	  @user_contacts = user_contacts.to_json
	  @country_data = countries_nested_data.to_json
	  @rank = Rank.all.order(:rowgroup_id,:order).map{|r| {id:r.id,name:r.name}}.to_json
	  @user = current_user.to_json
	  @redirect_back_to =  request.referrer || root_path
	end	
	 
	def user_communities
		user_communities = get_user_communities
		respond_to do |format|
		  format.json{render json: user_communities}
	  end 
	end	

	def save_communities
			all_communities = params[:communities]
			success = false
		if all_communities
		  all_communities = all_communities.select{|c| c[:is_owner]}
		  UserCommunity.transaction do 
			begin
				  all_communities.each do |com|
					success, comm = save_user_community com
				  end	
				rescue 
					raise ActiveRecord::Rollback
				end  
		  end
		end
		respond_to do |format|
		  format.json{render json: {success: success}}
		end 
	end	

  def delete_community
  	community_id = params[:id]
    @community = UserCommunity.find(community_id) if community_id
  	success = false

  	if @community && is_owner
       begin
       	 @community.destroy
       	 success = true
       rescue
       	 success = false
       end	
  	end	

  	respond_to do |format|
		  format.json{render json: {success: success}}
	  end 
  end

  def delete_community_user
  	community_id = params[:community_id].to_i
  	user_id = params[:contact_id].to_i
    @community = UserCommunity.find(community_id) if community_id > 0 && user_id > 0
  	success = false

  	if @community && is_owner
       begin
       	 community_contact = @community.user_community_contacts.detect{|c| c.contact_id==user_id}
       	 community_contact.destroy if community_contact.present?
       	 success = true
       rescue
       	 success = false
       end	
  	end	

  	respond_to do |format|
		  format.json{render json: {success: success}}
	  end 
  end

  def add_community_user
  	community_id = params[:community_id].to_i
  	user_id = params[:contact_id].to_i
    @community = UserCommunity.find(community_id) if community_id > 0 && user_id > 0
  	success = false

  	if @community && is_owner
       begin
       	 @community.user_community_contacts.create(contact_id: user_id)
       	 success = true
       rescue
       	 success = false
       end	
  	end	

  	respond_to do |format|
		  format.json{render json: {success: success}}
	  end 
  end
  	
	private
	def community_contacts_list community
			users_hash community.contacts
	end

	def users_hash users
		users.map{|user| user_hash(user) }
	end 

	def get_user_communities
		user = current_user
			user_communities = []
			if user
			  user.communities.each do |community|
				comm = {}
				comm[:id] = community.id
				comm[:name] = community.name
				comm[:color] = community.color
				comm[:owner] = user_hash community.user
				comm[:is_owner] = community.user_id == user.id ? true : false
				comm[:show_box] = false
				comm[:contacts] = community_contacts_list community
		  	user_communities << comm
			  end	
			end 
		user_communities
	end	

  def get_user_community community
		user = current_user
		comm = {}
		comm[:id] = community.id
		comm[:name] = community.name
		comm[:color] = community.color
		comm[:owner] = user_hash community.user
		comm[:is_owner] = community.user_id == user.id ? true : false
		comm[:show_box] = false
		comm[:contacts] = community_contacts_list community
		comm
  end	
 
  def save_user_community community
		if community[:id]
				comm = UserCommunity.find(community[:id])
		else
				comm = UserCommunity.new
		end	
			comm.name = community[:name]
		  comm.color = community[:color]
		if community[:owner].nil? || community[:owner][:originalObject].nil?
			comm.user_id =  current_user.id if comm.user_id.nil?
		else
			comm.user_id = community[:owner][:originalObject][:id]
		end	
		   

		if community[:contacts].present?
			contact_ids = community[:contacts].collect{|c| c[:id]}
			contact_ids.push(comm.user_id)
			comm.contact_ids = contact_ids.uniq
		else
			comm.contact_ids = [comm.user_id]
		end	
		success = comm.save
		return success,comm
  end	

  def is_owner
  	@community.owner.id == current_user.id
  end	
end
 