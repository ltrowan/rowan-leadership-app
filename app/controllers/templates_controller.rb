class TemplatesController < ApplicationController
	layout 'template'
  skip_before_filter :verify_authenticity_token
  skip_before_filter :authenticate_user!, only: [:show], :if =>lambda{ params[:guid].present?}
  before_action :set_redirect_back_to, only: [:show]

	def show		
		@user_template_id = params[:user_template_id]
		type = params[:type]

		if params[:guid].nil? 
			@is_review_template = false
		else
			@is_review_template = true
			@user_template = UserTemplate.find_by(guid:params[:guid].strip)
			@user_template_id = @user_template.try(:id)
		end	
		template = MasterTemplate.find_by(template_type: type.gsub('-','_').upcase)

		if template 
			@template_type = template.template_type					
			if @user_template_id.blank?
				#new template
				@template_data = ""
      else		
	    	#user prefilled template
	    	user_template = @user_template.nil? ? UserTemplate.find_by(id: @user_template_id, user_id: current_user.id, master_template_id: template.id) : @user_template
	    	@template_data = user_template.template_data
      end	
		end   

	end	

	def save
		template_type = params[:type]
		template_data = params[:template_data]
		user_template_id = params[:user_template_id]
		success = false
		result = {:success=>success}
		if template_type && template_data && current_user
	  	user_template = UserTemplate.find_by(id: user_template_id, user_id: current_user.id)
	  	template = MasterTemplate.find_by(template_type: template_type)
	  	if user_template.nil?
	  		user_template = UserTemplate.new(user_id: current_user.id, 
	  			                             master_template_id: template.id,
	  			                             template_data: template_data.to_json,
	  			                             name: template_data[:title])
	  	else
	  	   user_template.user_id = 	current_user.id
	  	   user_template.template_data = template_data.to_json
	  	   user_template.name = template_data[:title]
	  	end	
	  	if user_template.save
	  		result[:success] = true
	  		result[:user_template_id] = user_template.id
	  	end	
	  end		
    respond_to do |format|
      format.json do
        render :json => result
      end
    end
  end

  def destroy
    @user_template = UserTemplate.find(params[:id]).destroy
  end
  
  def send_template
		contacts_ids = params[:contacts]
		user_template_id = params[:user_template_id]
		result = {:success=>false}
		begin
		  if current_user && user_template_id && contacts_ids
	  	  user_template = UserTemplate.find_by(id: user_template_id, user_id: current_user.id)
	  	  if user_template
	  	  	if user_template.guid.nil?
	  	      user_template.guid = SecureRandom.uuid
	  	      user_template.save
	  	    end  
	  	    contacts = User.where(id: contacts_ids).collect{|u| u.email}.join(';')	  	    
	  	    result[:success] = true
	  	    UserTemplateMailer.template(contacts, user_template,request.base_url).deliver_later	  	    
	  	  end	
	    end		
	  rescue Exception => ex
	  	debugger
	  	result[:success] = false
	  end  
	  puts result
    respond_to do |format|
      format.json do
        render :json => result
      end
    end
  end
end
