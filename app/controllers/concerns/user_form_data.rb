module UserFormData
  extend ActiveSupport::Concern


  def load_form_data
    load_ranks
    @countries = Country.all
    load_states
    load_bases
  end

  def load_ranks
    allranks = Rank.all.order(:rowgroup_id,:order).group_by{|r| r.rowgroup_id}
    @ranks = []

    allranks.each do |key,val|
      @ranks << Rank.new(name:"---") if @ranks.length!=0
      val.each do |r|
        @ranks << r
      end
    end
  end

  def load_states
    country_id = params[:country_id] || resource.country_id
    @states = State.where(country_id: country_id)
  end

  def load_bases
    state_id = params[:state_id] || resource.state_id
    @bases = BaseLocation.where(state_id: state_id)
  end

end