class TabsController < ApplicationController

  before_action :save_tab_last_visited, only: [:show]

  def index
    @tabs = Tab.includes(:quick_links).order(:position)
  end
  
  def show
    @tab = Tab.includes(:images, :sections => :content_blocks).where(anchor_name: params[:anchor_name]).first
    @master_template = MasterTemplate.exclude_templates
    @user_templates = UserTemplate.where(user_id:current_user.id).includes(:master_template).group_by{|t| t.master_template.template_type} if !current_user.nil?
    @assessments = Assessment.all
    unless @tab.present?
      redirect_to root_path
    end
  end

  private
  def save_tab_last_visited
    session[:tab_last_visited] = request.path
  end


end
