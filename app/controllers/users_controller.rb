class UsersController < ApplicationController
  @@per_page = 10
  skip_before_filter :verify_authenticity_token, :only=>[:save_contacts]

  before_filter :authenticate_user!
  before_filter :load_user, only: [:add_contacts, :save_contacts, :add_community, :save_community]
  
  def add_contacts
    user = current_user
    @users = User.includes(:state, :country, :base_location)
                 .where(country_id: user.country_id, 
                        state_id: user.state_id,
                        base_location_id: user.base_location_id)
                 .where.not(id: user.id)
  end
  
  def save_contacts
    contact_ids = params[:contact_ids] || []
    contacts = contact_ids.map{|id| User.find(id)}
    @user.contacts << contacts
    success = @user.save
    respond_to do |format|
      format.json {render json: {success: success} }
      format.js
    end
  end
  
  def add_community
    @contacts = current_user.contacts
  end
  
  def save_community
    @community = UserCommunity.new(name: params[:name], color: "#ef4c7a", status: false, user_id: current_user.id)
    params[:contact_ids] = params[:contact_ids] || []
    if @community.save
      params[:contact_ids] << current_user.id
      @community.contact_ids = params[:contact_ids]
      @community.save
      respond_to do |format|
        format.js { }
      end
    end
  end
  
  def search_user
    user = current_user
    base_location_id = params[:base_location_id].nil? ? user.base_location_id : params[:base_location_id]
    country_id = params[:country_id].nil? ? user.country_id : params[:country_id]
    state_id = params[:state_id].nil? ? user.state_id : params[:state_id]
    rank_id = params[:rank_id].nil? ? user.rank_id : params[:rank_id]
    column_names = [{first_name: :word_start}, {last_name: :word_start}]
    page_number = params[:page_number]
    query = params[:query]
    @users = User.search( "#{query}" , fields: column_names,
      per_page: @@per_page, page: page_number,
      fielddata_fields: [:first_name, :last_name],
      where: {base_location_id: base_location_id,
              country_id: country_id, 
              state_id: state_id,
              rank_id: rank_id
      }
      ).map{|u|({first_name: u.first_name, last_name: u.last_name, id: u.id,
      image_url: u.image.url(:thumb), state_name: u.state.name, country_name: u.country.name, base_location: u.base_location.name}) unless u.id == user.id }.compact
    respond_to do |format|
      format.json {render json: @users }
      format.js
    end
  end

  def search_all_users
    query_where = {}
    query_where[:base_location_id] = params[:base_location_id].to_i if params[:base_location_id].to_i >0
    query_where[:country_id] = params[:country_id].to_i if params[:country_id].to_i >0
    query_where[:state_id] = params[:state_id].to_i if params[:state_id].to_i >0
    query_where[:rank_id] = params[:rank_id].to_i if params[:rank_id].to_i >0

    user = current_user
    column_names = [{first_name: :word_start}, {last_name: :word_start}]
    page_number = params[:page_number]
    query = params[:query]
    @users = User.search( "#{query}" , fields: column_names,
      per_page: @@per_page, page: page_number,
      fielddata_fields: [:first_name, :last_name],
      where: query_where
      ).map{|u|({first_name: u.first_name, last_name: u.last_name, id: u.id,
      image_url: ActionController::Base.helpers.image_path(u.image(:thumb)), rank_name: u.rank.name,state_name: u.state.name, country_name: u.country.name, base_name: u.base_location.name}) unless u.id == user.id }.compact
    
    respond_to do |format|
      format.json {render json: @users }
      format.js
    end
  end

  def contacts
    users = current_user.contacts.search_name(params[:query])
    respond_to do |format|
      format.json{render json: search_results(users) , status: :ok}
    end
  end
  
  def confirm
    token = params[:token]
    user_id = params[:user]
    user = User.where(confirmation_token: token)
    if user.present?
      user[0].id = user_id
      user[0].confirmed_at = Time.now
      user[0].save
      redirect_to root_url
    else
      render plain: 'invalid token'
    end
  end

  def report_bug
    UserMailer.bug_report(current_user, params[:bug][:message]).deliver_later
  end
  
  protected
  def load_user
    @user = User.find_by_id(params[:id])
    unless @user
      render :not_found
    end
  end

  def search_results users
    {results: users.map{|user| {id: user.id, first_name: user.first_name, last_name: user.last_name, email: user.email,
                                image: ActionController::Base.helpers.image_path(user.image(:thumb))} }}
  end

end