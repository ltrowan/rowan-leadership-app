class ContentBlockPostsController < InheritedResources::Base
  skip_before_action :verify_authenticity_token, only: :webhook_notify
  skip_before_filter :authenticate_user!, only: :webhook_notify

  def create
    case params[:post_type].to_i
    when ContentBlockPost.post_types[:text]
    	@postable = PostText.create(content: params[:text])
    when ContentBlockPost.post_types[:link]
      # Links not working without http:// in front of them
      # Quick fix here
      url = params[:url]
      if /^https?:\/\//.match(url).nil?
        url = "http://" + url
      end
      @postable = PostLink.create(url: url, description: params[:description])
    when ContentBlockPost.post_types[:video]
      @postable = PostVideo.where("title IS NULL AND user_id = ?", current_user.id).last
      @postable.update_attributes(title: params[:title])
      #@postable = PostVideo.create(title: params[:title], video: params[:video])
    end
    @post = ContentBlockPost.create(
      content_block_id: params[:content_block_id],
      user_id: current_user.id,
      postable_id: @postable.id,
      postable_type: @postable.class.name,
      community_id: params[:community_id]
    )

  end
  
  def new_reply
    @reply = Reply.new
  end
  

  def save_reply
    reply = Reply.create(user_id: current_user.id, content_block_post_id: params[:content_block_post_id], content: params[:reply][:content])
    render json: { success: true }
    rescue 
      render json: { success: false }
  end
  
  def replies
    @post = ContentBlockPost.find(params[:content_block_post_id])
    @replies = @post.replies
    render json: @replies
    rescue 
      render json: { }
  end

  def index
    @content_block_posts = ContentBlockPost.includes(:postable, replies: :user)
    .where(content_block_id: params[:content_block_id]).where(community_id: community_ids)

    @content_block = ContentBlock.find(params[:content_block_id])

    init_contacts_and_communities

    respond_to do |format|
      format.js
      format.json {render json: @content_block_posts.map{|p| p.postable}}
    end
  end
  
  def index_content_block
    @content_block = ContentBlock.find(params[:id])
    posts = @content_block.content_block_posts.includes(:postable)
    render json: posts
    rescue 
      render json: { } 
  end
  
  def contacts_and_communities
    init_contacts_and_communities
    respond_to do |format|
      format.json {render json: @contacts_and_communities }
      format.html {render 'contacts_and_communities'}
      format.js
    end

  end
  
  def email
    content_block = ContentBlock.find(params[:id])
    content_block_link = "#{params[:current_tab_url]}##{content_block.anchor_name}"
    contacts_ids, communities_ids = contacts_and_communities_arrays
    communities = UserCommunity.find(communities_ids)
    users = User.find(contacts_ids)
    users_com = communities.map{|com| com.contacts.to_a }.flatten
    contacts = users + users_com
    contacts.uniq!
    if contacts.length > 0 && params[:email][:body].present?
      contacts.each do |contact|
        PostMailer.post(contact, params[:email][:body], content_block_link).deliver_later
      end
      redirect_to contacts_and_communities_content_block_path(params[:id])
    end

  end

  def destroy
    @content_block_post = ContentBlockPost.find(params[:id]).destroy
  end

  def webhook_notify
    if request.post?
    #  if Rails.env == "development"
    #    params = {"payload"=>"{\"version\":\"1.0\",\"event\":\"video_transcoded\",\"data\":{\"duration\":14,\"audioCodec\":\"AAC\",\"videoCodec\":\"H.264\",\"type\":\"MP4\",\"size\":25788,\"width\":320,\"height\":240,\"orientation\":\"landscape\",\"id\":\"2295\",\"url\":\"http:\\/\\/s1.addpipe.com\\/v\\/78e8a3a7f7194d4e357a6de8eee68122\\/vs1437059227877_963.mp4\",\"snapshotUrl\":\"https:\\/\\/s1.addpipe.com\\/v\\/78e8a3a7f7194d4e357a6de8eee68122\\/vs1437059227877_963.jpg\",\"payload\":\"user_id=1\"}}"}
    #  end
      @response   = JSON.parse( params["payload"] )
      user_param  = @response["data"]["payload"]
      video_url   = @response["data"]["url"]

      u_id        = user_param.split('=').last
      user        = User.find u_id
      video_stat  = VideoStatus.where(user_id: u_id, processed: false).last # Rails.env == "development" ? VideoStatus.last : VideoStatus.where(user_id: u_id, processed: false).last
      video_url   = video_url.split("/")
      video_name  = video_url.last
      file_name   = video_name
      file_size   = ''
      file_type   = ''
      tries       = 0
      aws_data    = Rails.application.secrets[:aws]
      s3 = Aws::S3::Client.new() #access_key_id: 'AKIAIFF4IQVIKYMEXT5Q', secret_access_key: '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'
      begin
        resp = s3.get_object( bucket: aws_data['S3_BUCKET_NAME'], key: video_name ) #do | chunk |
        file_size = resp.content_length
        file_type = resp.content_type
      rescue Aws::S3::Errors::NoSuchKey => e
        tries += 1
        Rails.logger.info "e...... #{e.inspect}"
        retry if tries <= 5
        Rails.logger.info "no dice!"
      end

      if user && video_stat
        if video_stat.video_type == "post"
          @postable = PostVideo.new( video_file_name: file_name, video_content_type: file_type, video_file_size: file_size, video_updated_at: Time.now, user_id: user.id )
        else
          @content_block_post = ContentBlockPost.find(video_stat.content_block_post_id)
          @postable = @content_block_post.replies.build(content: " ", video_file_name: file_name, video_content_type: file_type, video_file_size: file_size)
          @postable.user = user
        end

        if @postable.save
          video_stat.update_attributes(processed: true)
          Rails.logger.info "@postable+++++++++++ #{@postable.inspect}"
        else
          Rails.logger.info "@postable errors++++++++++++++#{@postable.errors.inspect}"
        end
      end

      respond_to do | format |
        format.html { render text: "Payload received and parsed successfully." }
        format.json { render json: "Payload received and parsed successfully." }
      end
    end
  end

  def check_video_upload_status
    @video_status = VideoStatus.where(user_id: current_user.id, processed: true).last
    key = 0
    if @video_status.video_type == "reply"
      @reply = Reply.where(user_id: current_user.id, content_block_post_id: @video_status.content_block_post_id).last
      key = @reply.id if @reply
    end

    respond_to do | format |
      if @video_status
        format.html { render text: @video_status.video_type + "_" + key.to_s }
      else
        format.html { render text: '' }
      end
    end
  end

  def modify_video_params
    @video_status = VideoStatus.new(user_id: current_user.id, video_type: params[:type], content_block_post_id: params[:content_block_post])

    respond_to do |format|
      if @video_status.save
        format.html { render text: true }
        format.json { render json: true }
      else
        format.html { render text: @video_status.errors }
        format.json { render json: @video_status.to_json }
      end
    end
  end

  private

  def contacts_and_communities_arrays
    contacts = {}
    (params[:contacts]||[]).map{ |y| y.split("-")}.each do |values|
      key = values[0]
      contacts[key] = []  unless contacts[key].present?
      contacts[key] << values[1]
    end
    [(contacts["contacts"]||[]), (contacts["communities"]||[])]
  end

    def content_block_post_params
      params.require(:content_block_post).permit(:content_block_id, :user_id, :postable_id, :postable_type, :content, :url, :desc )
    end


  def community_ids
    params[:filter_present] ? (params[:search][:community_ids] rescue []) : current_user.communities.collect(&:id)
  end

end
