ActiveAdmin.register LeaderProfessionalDevelopment, as: "Lpd" do
  menu priority: 30
  config.comments = false
  
  config.filters = false

  permit_params :title, :subtitle, :background_image
  
  index do |lpd|
    column "Title" do |lpd|
      link_to lpd.title, admin_lpd_concepts_path(lpd)
    end
    column "SubTitle" do |lpd|
      lpd.subtitle
    end
    column "Edit" do |lpd|
      link_to("Edit", edit_admin_lpd_path( lpd ))
    end
    column "Delete" do |lpd|
      link_to("Delete", admin_lpd_path( lpd ),
        method: :delete, data: { confirm: 'Are you sure?' } )
    end
  end

  form :html => { :enctype => "multipart/form-data" } do |f|
    f.inputs "Tab Details" do
      f.input :title
      f.input :subtitle
      f.input :background_image, :as => :file, :hint => f.object.background_image_file_name
      f.actions
    end
  end
  show do |lpd|
    div :class => "panel" do
      h3 "Conepts"
      if lpd.concepts and lpd.concepts.count > 0
        div :class => "panel_contents" do
          div :class => "attributes_table" do
            table do
              tr do
                th do
                  "Name"
                end
              end
              tbody do
                lpd.concepts.order(:position).each do |concept|
                  tr do
                    td do
                      link_to concept.name, admin_lpd_concept_path(lpd_id: lpd.id, id: concept.id )
                    end
                  end
                end
              end
            end
          end
        end
      else
        h3 "No Section available"
      end
    end
    div :class => "panel_contents" do
      link_to 'New Concept', new_admin_lpd_concept_path(lpd_id: lpd.id)
    end
  end
end
