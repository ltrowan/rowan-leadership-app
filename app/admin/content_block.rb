ActiveAdmin.register ContentBlock do
  config.sort_order = 'position_asc'
  config.filters = false
  config.comments = false
  
  actions :all
  menu false
  
  index do
    div class: :header, id: "header", style: "height:5px"do
      "Sections"
    end
    div :class => "panel_contents" do
      link_to("Background Images", admin_tab_images_path(params[:tab_id]) )
    end
    div :class => "panel" do
      link_to("Overview", edit_admin_tab_path(params[:tab_id]))
    end
    div :class => "panel_contents" do
      link_to("Table Of Contents", "#")
    end
    column "Title", :title
    column "Type", :block_type
    column "Modified", :updated_at
    column "Move Up" do |content_block|
      link_to("MoveUP", moveup_admin_content_block_path(content_block.id)) unless content_block.first?
    end
    column "Move Down" do |content_block|
      link_to("MoveDown", movedown_admin_content_block_path(content_block.id )) unless content_block.last?
    end
    column "Edit" do |content_block|
      link_to("Edit", edit_admin_tab_section_content_block_path(params[:tab_id], params[:section_id], content_block.id))
    end
    column "Delete" do |content_block|
      link_to("Delete", admin_tab_section_content_block_path(params[:tab_id], params[:section_id], content_block.id),
        method: :delete, data: { confirm: 'Are you sure?' } )
    end
  end
  
  form :html => { :enctype => "multipart/form-data" } do |f|
    render 'form'
  end
  

  controller do
    nested_belongs_to :tab, :section
    
    def new
      @errors = params[:errors]
      @type = params[:type]
      @cb = params[:cb]
      @type_data = params[:type_data]       
      super
    end
    
    def get_form_partial
      @s3_direct_post = S3_BUCKET.presigned_post(key: "uploads/#{SecureRandom.uuid}/${filename}", 
        success_action_status: '201'
      )
      @content_block = (params[:cb] == "null")? Section.find(params[:section_id]).content_blocks.new :
        JSON.parse(params[:cb])       
      # check to render error 
      (params[:errors] == "null")? @errors = [] : @errors = JSON.parse(params[:errors])
      
      @partial = get_partial(params[:type])
      # used at th model side to save type
      @value = params[:type]
      #value to initilize the type of selcted value
      @lpds = LeaderProfessionalDevelopment.all
      type = AdminData::Block::Types[(params[:type].to_i - 1)][0] 
      @parent_anchor = params[:tab_id].nil? ? "" : Tab.find(params[:tab_id]).anchor_name 
      @block = (params[:type_data] == "null")? type.constantize.new : 
        JSON.parse(params[:type_data])       
    end
    
    def create
      result = ContentBlock.save_block(params)
      @content_block = result[0]
      type_data = result[1]
      if @content_block.errors.messages.blank?
        redirect_to admin_tab_section_content_blocks_path(params[:tab_id], params[:section_id]), 
          notice: 'Content block was successfully created.' 
      else
        redirect_to new_admin_tab_section_content_block_path(params[:tab_id], params[:section_id], 
          errors: @content_block.errors.messages, type: params[:type], 
          cb: @content_block.attributes, type_data: type_data.attributes)
      end
    end
    
    def edit
      @errors = params[:errors]
      @cb = params[:cb]
      @type_data = params[:type_data]
      super
    end 
    
    def get_edit_form_partial
      @content_block = (params[:cb] == "null")? ContentBlock.find(params[:id]) : 
        JSON.parse(params[:cb])
      
      @type = AdminData::Block::Types.find{|type| type[0] == @content_block["block_type"]}
      (params[:errors] == "null")? @errors = [] : @errors = JSON.parse(params[:errors])
      @value = @type[1]
      @block = (params[:type_data] == "null")? @content_block.block : 
        JSON.parse(params[:type_data])
        @parent_anchor = params[:tab_id].nil? ? "" : Tab.find(params[:tab_id]).anchor_name  
      @lpds = LeaderProfessionalDevelopment.all
      @partial = get_partial(@value)      
    end
    
    def update
      result = ContentBlock.save_block(params)
      @content_block = result[0]
      type_data = result[1]
      respond_to do |format|
        if @content_block.errors.messages.blank?
          format.html { redirect_to admin_tab_section_content_blocks_path(params[:tab_id], params[:section_id]),
           notice: 'Content block was successfully updated' }
        else
          format.html {redirect_to edit_admin_tab_section_content_block_path(params[:tab_id], params[:section_id], 
            params[:id], :errors => @content_block.errors.messages,
            cb: @content_block.attributes, type_data: type_data.attributes)}
        end
      end
    end
    

  private    
    def get_partial(type)
      case type.to_i
        when AdminData::Block::TextBox
          partial = 'form_text_box'
        when AdminData::Block::Title
          partial = 'form_title'    
        when AdminData::Block::Template
          partial = 'form_template'
        when AdminData::Block::VideoHorizontal
          partial = 'form_video_horizontal'
        when AdminData::Block::VideoVertical
          partial = 'form_video_vertical'
        when AdminData::Block::Ttp
          partial = 'form_ttp'
        when AdminData::Block::Introduction
          partial = 'form_introduction'
        when AdminData::Block::Lpd
          partial = 'form_lpd'
        else
      end
    end
  end
  
  member_action :moveup, :method => :get do
    resource = ContentBlock.find(params[:id])
    resource.move_higher
    redirect_to admin_tab_section_content_blocks_path( resource.section.tab_id, resource.section_id ), :notice => "Moved!"
  end

  member_action :movedown, :method => :get do
    resource = ContentBlock.find(params[:id])
    resource.move_lower
    redirect_to admin_tab_section_content_blocks_path( resource.section.tab_id, resource.section_id ), :notice => "Moved!"
  end
end