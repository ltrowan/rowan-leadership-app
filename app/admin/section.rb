ActiveAdmin.register Section do
  belongs_to :tab
  permit_params content_blocks_attributes: [ :id, :position, :url, :block_type, 
    :block_id, :anchor_name, :width, :justification, :alpha, :spacing ]
  menu false
  menu :if => proc{false}
  config.comments = false

  index do |section|
    column "Chapter Name" do |section|
      link_to section.title, admin_tab_section_content_blocks_path(params[:tab_id], section.id)
    end
  end
  config.filters = false
  
end
