ActiveAdmin.register Question do
  config.sort_order = 'position_asc'
  belongs_to :assessment
  config.filters = false
  config.comments = false
  menu false
  menu :if => proc{false}
  permit_params :title, :stops_count, :position

  index do |question|
    column "Title" do |question| 
      link_to question.title, 
        admin_assessment_question_path(params[:assessment_id], question.id)
    end 
    column "Move Up" do |question|
      link_to("MoveUP", moveup_admin_assessment_question_path(params[:assessment_id], question.id)) unless question.first?
    end
    column "Move Down" do |question|
      link_to("MoveDown", movedown_admin_assessment_question_path(params[:assessment_id], question.id )) unless question.last?
    end
    column "Delete" do |question|
      link_to("Delete", 
        admin_assessment_question_path(params[:assessment_id], question.id),
        method: :delete, data: { confirm: 'Are you sure?' } )
    end
  end
  form :html => { :enctype => "multipart/form-data" } do |f|
    f.inputs "Question" do
      f.input :title
      f.input :position
      f.input :stops_count, :label => 'Stop', as: :select, 
        collection: [3,5,7],  include_blank: false
    end
    f.actions
  end
  
  show title: :title do |question|
    div :class => "panel_contents" do
      div :class => "inputs" do
        tbody do
          render 'form_stops'
        end
      end
    end
  end
  
  controller do
    def update
      update! do |format|
        format.html { redirect_to admin_assessment_questions_path(params[:assessment_id]) } if resource.valid?
      end
    end

    def create
      create! do |format|
        format.html {redirect_to admin_assessment_questions_path(params[:assessment_id]) } if resource.valid?
      end
    end
  
    def save_stops
      @question = Question.find(params[:id])
      @question.intro_text = params[:intro_text]
      @question.question_text = params[:question_text]
      @question.save
      if @question.stops.count != @question.stops_count
        @question.stops_count.times do |i|
          stop = @question.stops.new
          stop.text = params["stop_text_#{i}"]
          stop.score = params["score_#{i}"]
          stop.save
        end
      else
        @question.stops.each_with_index do |stop, i|
          stop.text = params["stop_text_#{i}"]
          stop.score = params["score_#{i}"]
          stop.save
        end      
      end
      redirect_to admin_assessment_questions_path(params[:assessment_id])
    end
  end
  member_action :moveup, :method => :get do
    resource = Question.find(params[:id])
    resource.move_higher
    redirect_to admin_assessment_questions_path(params[:assessment_id]), :notice => "Moved!"
  end

  member_action :movedown, :method => :get do
    resource = Question.find(params[:id])
    resource.move_lower
    redirect_to admin_assessment_questions_path(params[:assessment_id]), :notice => "Moved!"
  end
end
