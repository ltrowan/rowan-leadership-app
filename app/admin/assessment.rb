ActiveAdmin.register Assessment do
  menu priority: 50
  config.filters = false
  config.comments = false
  permit_params :title, :anchor_name, :time, :description, :sub_title, 
    :tag_list
  
  index do |assessment|
    column 'Assessment Name', :title
    column 'Anchor', :anchor_name
    column :url
    column "Edit" do |assessment|
      link_to("Edit", admin_assessment_path(assessment))
    end
    column "View" do |assessment|
      link_to("View", assessment_path(assessment))
    end
    column "Delete" do |assessment|
      link_to("Delete", admin_assessment_path(assessment),
        method: :delete, data: { confirm: 'Are you sure?' } )
    end
  end
  
  form do |f|
    f.inputs "Assesmnet Details" do
      f.input :title, label: 'Assessment Name'
      f.input :anchor_name
      f.input :sub_title
      f.input :time
      f.label "Description*", :class => "string input required stringish"
      f.input :description
      f.input :tag_list, :as => :tag_list, :label => "Tags"
      actions
    end
  end

  show do |assessment|
    div :class => "attributes_table" do
    table do
        tbody do
          tr do
            td do
              link_to "Edit Core Information", edit_admin_assessment_path(assessment)
            end
          end
          tr do  
            td do
              link_to "Edit Score Ranges", admin_assessment_score_ranges_path(assessment.id)
            end
          end
          tr do
            td do  
              link_to "Edit Questions", admin_assessment_questions_path(assessment.id)
            end
          end
          tr do  
            td do  
              link_to "Edit Media", admin_assessment_media_path(assessment.id)
            end
          end
        end  
      end
    end
  end
end
