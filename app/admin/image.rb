ActiveAdmin.register Image do
  
  permit_params :pc_image, :mobile_image, :height, :tab_id, :position
  config.filters = false
  config.comments = false
  belongs_to :tab, :optional => false
  menu :if => proc{false}
  config.sort_order = 'position_asc'
  #config.batch_actions = false
  actions :all  
  
  index  do |image|
    selectable_column
    column "ImageThumb" do |image| 
     link_to image_tag(image.pc_image(:thumb)), 
        edit_admin_tab_image_path(params[:tab_id], image), html: {style: 'width:100px'}
    end 
  end
  
  form :html => { :enctype => "multipart/form-data" } do |f|
    f.inputs "Images" do
      f.input :pc_image, :label => 'Web Image',:as => :file, :hint => f.object.pc_image_file_name
      f.input :mobile_image, :label => 'Mobile Image',:as => :file, :hint => f.object.mobile_image_file_name
      f.input :height, :label => 'Percent Of Total Height %', input_html: {style: 'width:70px'}
      f.input :position, :label => 'Image order', input_html: {style: 'width:70px'}
    end
    f.actions
  end
  
  show do |image|
    div do 
      image_tag(image.pc_image)
    end
    div do
      image_tag(image.mobile_image)
    end
  end 
  
  controller do
    def update
      update! do |format|
        format.html { redirect_to admin_tab_images_path(params[:tab_id]) } if resource.valid?
      end
    end

    def create
      create! do |format|
        format.html {redirect_to admin_tab_images_path(params[:tab_id]) } if resource.valid?
      end
    end
  end 
  
end
