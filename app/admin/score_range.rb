ActiveAdmin.register ScoreRange do
  belongs_to :assessment
  config.filters = false
  config.comments = false
  menu false
  menu :if => proc{false}
  permit_params :start, :ending, 
    capabilities_attributes: [:sub_title, :main_text]
  
  index do |score_range|
    column "Ranges" do |score_range|
      link_to score_range.range, 
        admin_assessment_score_range_path(params[:assessment_id], score_range)
    end
  end
  
  show title: :range do |score_range|
    div :class => "panel_contents" do
      div :class => "inputs" do
        tbody do
          render 'form_capabilities'
        end
      end
    end
  end
  controller do
    def update
      update! do |format|
        format.html { redirect_to admin_assessment_score_ranges_path( params[:assessment_id] ) } if resource.valid?
      end
    end

    def create
      create! do |format|
        format.html {redirect_to admin_assessment_score_ranges_path( params[:assessment_id] ) } if resource.valid?
      end
    end
    # gb means gather_feedback
    def show
      @gb_errors = (params["gb_errors"].nil?)?  [] : params["gb_errors"]
      @study_errors = (params["study_errors"].nil?)?  [] : params["study_errors"]
      @practice_errors = (params["practice_errors"].nil?)?  [] : params["practice_errors"]
      @score_range = ScoreRange.find(params[:id])
      @capabilities = @score_range.capabilities
      if params["study"].blank?
        if @capabilities.study.blank?
          @study = @capabilities.study.new
        else
          @study = @capabilities.study[0]
        end
      else
        @study = params["study"]
      end
      if params["practice"].blank?
        if @capabilities.practice.blank?
          @practice = @capabilities.practice.new
        else
          @practice = @capabilities.practice[0]
        end
      else
        @practice = params["practice"]
      end
      if params["gb"].blank?
        if @capabilities.gather_feedback.blank?
          @gb = @capabilities.gather_feedback.new
        else
          @gb = @capabilities.gather_feedback[0]
        end
      else
        @gb = params["gb"]
      end
    super
    end
    def save_capability
      result = ScoreRange.save_capability(params)
      if !result[3]
        redirect_to admin_assessment_score_ranges_path( params[:assessment_id] )
      else
        redirect_to admin_assessment_score_range_path( params[:assessment_id], 
          params[:id], 
          study: result[0].attributes, study_errors: result[0].errors.messages,
          practice: result[1].attributes, practice_errors: result[1].errors.messages,
          gb: result[2].attributes, gb_errors: result[2].errors.messages
        )
      end
    end
  end

end
