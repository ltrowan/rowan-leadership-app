ActiveAdmin.register Medium do
  belongs_to :assessment
  config.filters = false
  config.comments = false
  menu false
  permit_params :file
  actions :all, except: [:show]
  menu :if => proc{false}
  form :html => { :enctype => "multipart/form-data" } do |f|
    f.inputs "Media" do
      f.input :file, :as => :file, :hint => f.object.file_file_name
    end
    f.actions
  end
  
  index do |medium|
    column "File Name" do |medium|
      link_to medium.file_file_name, 
        edit_admin_assessment_medium_path( params[:assessment_id], medium.id)
    end
    column "File Content Type" do |medium|
      medium.file_content_type
    end
    column "Delete" do |medium|
      link_to("Delete", 
        admin_assessment_medium_path(params[:assessment_id], medium.id),
        method: :delete, data: { confirm: 'Are you sure?' } )
    end
  end
  controller do 
    def update
      update! do |format|
        format.html { redirect_to admin_assessment_media_path( params[:assessment_id] ) } if resource.valid?
      end
    end

    def create
      create! do |format|
        format.html {redirect_to admin_assessment_media_path( params[:assessment_id] ) } if resource.valid?
      end
    end
  end
end