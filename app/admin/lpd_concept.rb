ActiveAdmin.register LpdConcept, as: "Concept" do
  config.comments = false
  config.filters = false
  config.sort_order = 'position_asc'

  belongs_to :leader_professional_development, parent_class: LeaderProfessionalDevelopment, 
    :param => :lpd_id

  index do |concept|
    column "Tabs" do |concept|
      link_to concept.name, edit_admin_lpd_concept_path(params[:lpd_id], concept.id)
    end
    
    column "Move Up" do |concept|
      link_to("MoveUP", moveup_admin_lpd_concept_path(params[:lpd_id], concept.id)) unless concept.first?
    end
    
    column "Move Down" do |concept|
      link_to("MoveDown", movedown_admin_lpd_concept_path(params[:lpd_id], concept.id )) unless concept.last?
    end
    column "Delete" do |concept|
      link_to("Delete", admin_lpd_concept_path(params[:lpd_id], concept.id),
        method: :delete, data: { confirm: 'Are you sure?' } )
    end
  end

  form :html => { :enctype => "multipart/form-data" } do |f|
    render 'form'
  end
  
  controller do
    before_filter :set_params
    
    def set_params
      params[:leader_professional_development_id] = params[:lpd_id]
    end
    
    def index
      super
    end
    
    def new
      @concept = LpdConcept.new
      @leader_professional_development = LeaderProfessionalDevelopment.find(params[:lpd_id])
      @s3_direct_post = S3_BUCKET.presigned_post(key: "uploads/#{SecureRandom.uuid}/${filename}", 
        success_action_status: '201'
      )
    end
    
    def create
      @concept = LpdConcept.new
      @concept.name = params[:name]
      @concept.description = params[:description]
      @concept.video_url = params[:video]
      @concept.leader_professional_development_id = params[:lpd_id]
      @concept.save
      @concept.move_to_bottom
      redirect_to admin_lpd_path( params[:lpd_id] )
    end
    
    def edit
      @leader_professional_development = LeaderProfessionalDevelopment.find(params[:lpd_id]) 
      @concept = LpdConcept.find(params[:id])
      @s3_direct_post = S3_BUCKET.presigned_post(key: "uploads/#{SecureRandom.uuid}/${filename}", 
        success_action_status: '201'
      )
    end
    
    def update
      @concept = LpdConcept.find(params[:id])
      @concept.name = params[:name]
      @concept.description = params[:description]
      @concept.video_url = params[:video] if !params[:video].blank?
      @concept.leader_professional_development_id = params[:lpd_id]
      @concept.save
      redirect_to admin_lpd_path( params[:lpd_id] )
    end
  end
  
  member_action :moveup, :method => :get do
    resource = LpdConcept.find(params[:id])
    resource.move_higher
    redirect_to admin_lpd_concepts_path( resource.lpd.id ), :notice => "Moved!"
  end

  member_action :movedown, :method => :get do
    resource = LpdConcept.find(params[:id])
    resource.move_lower
    redirect_to admin_lpd_concepts_path( resource.lpd.id ), :notice => "Moved!"
  end
  
end
