ActiveAdmin.register Tab do
  
  menu false
  
  permit_params :title, :anchor_name, :time,
    :frame_size, :description, :background_image, :position,
    quick_links_attributes: [ :id, :position, :title, :_destroy],
    sections_attributes: [ :id, :position, :title, :time, :_destroy],
    images_attributes: [:pc_image, :mobile_image, :height, :id, :_destroy],
    tab_links_attributes: [:id, :url, :title, :position, :tag_list, :_destroy]
  
  config.comments = false
  config.sort_order = 'position_asc'

  index do |tab|
    column "Tabs" do |tab|
      link_to tab.title, edit_admin_tab_path(tab)
    end
  end
  config.filters = false

  form :html => { :enctype => "multipart/form-data" } do |f|
    f.inputs "Tab Details" do
      f.input :title
      f.input :anchor_name, {:input_html => { :class => 'anchor_box',:onkeyup=>"update_url(this,'url_value')"}}
      f.li<< content_tag(:div,:class=>"url_row") do
                '<label class="label">URL</label> <label class="url_value"></label>'.html_safe
             end
      f.input :frame_size
      f.input :position
      f.input :time
      f.inputs "Description" do
        f.input :description, 
          :input_html => { :class => "tinymce", :rows => 1, :cols => 2, :maxlength => 10 }
      end
      f.input :background_image, :as => :file, :hint => f.object.background_image_file_name
      f.inputs "Quick Links" do
        f.has_many :quick_links, sortable: :position, new_record: true, allow_destroy: true do |link|
          link.input :title
        end
      end
      f.actions
    end
  end

  show do |tab|
    div :class => "panel" do
      h3 " Chapter Name"
      if tab.sections and tab.sections.count > 0
        div :class => "panel_contents" do
          div :class => "attributes_table" do
            table do
              tr do
                th do
                  "Section Text"
                end
              end
              tbody do
                tab.sections.each do |section|
                  tr do
                    td do
                      link_to section.title, admin_tab_section_content_blocks_path(tab.id, section.id)
                    end
                  end
                end
              end
            end
          end
        end
      else
        h3 "No Section available"
      end
    end
    # to save sections on tab edit form
    div :class => "panel" do
      h3 "Sections"
      div :class => "panel_contents" do
        div :class => "inputs" do
          tbody do
            active_admin_form_for [:admin, tab] do |f| 
              f.has_many :sections, sortable: :position,
                new_record: true, allow_destroy: true do |section|
                section.input :title
                section.input :time
              end
              f.actions   
            end
          end
        end
      end
    end
    #to show added sections
    div :class => "panel" do
      h3 "Links"
      div :class => "panel_contents" do
        div :class => "inputs" do
          tbody do
            active_admin_form_for [:admin, tab] do |f| 
              f.has_many :tab_links, sortable: :position, 
                new_record: true, allow_destroy: true do |link|
                link.input :title
                link.input :url
                link.input :tag_list, :as => :tag_list, :label => "Tags"
              end
              f.actions   
            end
          end
        end
      end
    end
  end
end