class PostMailer < ActionMailer::Base
  default from: "noreply@leadership.rowan.nyc <noreply@leadership.rowan.nyc>"
  
  def post(contact, content, content_block_link)
    @content_block_link = content_block_link
    @content = content
    mail(to: contact.email, subject: 'Post from Rowan Site')
  end
end