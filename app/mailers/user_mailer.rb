class UserMailer < ActionMailer::Base
   default from: "noreply@leadership.rowan.nyc <noreply@leadership.rowan.nyc>"
  def welcome_email(user)
    @user = user
    @token = user.confirmation_token
    @name = user.name
    mail(to: user.miltary_email, subject: 'Welcome to Rowan Site')
  end

  def bug_report(user, message)
   @user = user
   @message = message
   mail(to: "bugs@rowan.nyc", subject: "#{@user.rank_full_name} has reported a bug")
  end

end