class UserTemplateMailer < ActionMailer::Base
  default from: "noreply@leadership.rowan.nyc <noreply@leadership.rowan.nyc>"
  
  def template(contacts, user_template, base_url)  	 
  	@user_template_url = "#{base_url}/templates/#{user_template.master_template.template_type.gsub('_','-')}/?guid=#{user_template.guid}"
    mail(bcc: contacts, subject: user_template.master_template.name)
  end
end