module AssessmentsHelper

  def get_label_alignment(index, length)
    middle_el = (length/2)+1;
    if index < middle_el
      'left'
    elsif index > middle_el
      'right'
    else
      'center'
    end
  end
  
end
