module TemplatesHelper
  def master_templates
    MASTER_TEMPLATES.each do |_master_template|
      content_tag :li do
        link_to _master_template, "javascript:void(0)"
        content_tag :div, class: ["sub-section-block"] do
          (@user_templates[_master_template.downcase.gsub(" ", "_").to_sym] || []).each do |_user_template|
            link_to _user_template.title, ""
          end
        end
      end
    end
  end
end
