module ApplicationHelper

  def rgb2hex(input, opacity=100)
    match = /^\s*rgba?\(\s*(\d{1,3}),\s*(\d{1,3}),\s*(\d{1,3})(?:,[\d\.]+)?\s*\)(\s*;?)/.match(input)
    if !match.nil? # RGB -> Hex
      res = "#"
      short_safe = true
      matches = [match[1], match[2], match[3]].map do |m|
        h = m.to_i.to_s(16)
        if h.length == 1
          "0#{h}"
        else
          short_safe = short_safe && h[0] == h[1]
          h
        end
      end
      if short_safe
        matches = matches.map do |m|
          m.split("")[0]
        end
      end
      res << matches.join("")
      res << match[4]
      res.upcase!
    else
      match = /^#([A-F0-9]{1,2})([A-F0-9]{1,2})([A-F0-9]{1,2})(\s*;?)$/i.match(input)
 
      if match.nil? # Bad input
        res = input
      else # Hex -> RGB
        res = "rgba("
        res << [match[1], match[2], match[3]].map do |m|
          m << m if m.length == 1
          m.hex.to_s
        end.join(",")
        res << ","
        res << (opacity/100.to_f).to_s
        res << ")"
        res << match[4]
      end
  
    end
    return res
  end
  
  def decrypt_delimeters_and_return_html(content)
    html = content.gsub("&lt\;", "<").gsub("&gt\;", ">")
    audio_tags = html.scan(/<audio>.+?<\/audio>/)
    link_tags = html.scan(/<link>.+?<\/link>/)
    assessment_tags = html.scan(/<assessment>.+?<\/assessment>/)
    audio_tags.each{|aud| html.gsub!("#{aud}", replace_audio_tag(aud))}
    link_tags.each{|link| html.gsub!("#{link}", replace_link_tag(link))}
    assessment_tags.each{|assessment| html.gsub!("#{assessment}", replace_assessment_tag(assessment))}
    return html
  end
  
  def replace_audio_tag(tag)
    link = tag.gsub("<audio>", "").gsub("</audio>", "")
    return "<audio controls preload = 'metadata'><source src='#{link}'></audio>"
  end
  
  def replace_link_tag(tag)
    link = tag.gsub("<link>", "").gsub("</link>", "")
    return "<a href='#{link}' target='_blank' class='fa fa-link'></a>"
  end
  
  def replace_assessment_tag(tag)
    assessment_link = tag.gsub("<assessment>", "").gsub("</assessment>", "")
    assessment_link.gsub!(/^ */, "")
    assessment_link.gsub!(/ *$/, "")
    uri = URI::parse(assessment_link)
    return "<a href='#{uri.path}' class='assessment-link'></a>"
  end

  def with_new_lines input
      input.gsub(/\n/, '<br/>').html_safe
  end

  def comments_border_color community
    "border-right: 4px solid #{community.try(:color)}"
  end

  def template_url template_type
      "/templates/#{template_type.gsub('_','-').downcase}"
  end  
end
