require File.expand_path('../boot', __FILE__)

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module RowantechLeadership
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    # config.time_zone = 'Central Time (US & Canada)'
    config.autoload_paths << Rails.root.join('lib')
    aws_data = Rails.application.secrets[:aws]
    config.paperclip_defaults = {
      :storage => :s3,
      #:s3_host_name => S3_URL,
      :s3_credentials => {
        :bucket => aws_data['S3_BUCKET_NAME'],
        :access_key_id => aws_data['S3_ACCESS_KEY'],
        :secret_access_key => aws_data['S3_SECRET']
      }
    }
    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    # config.i18n.default_locale = :de
    config.active_record.raise_in_transactional_callbacks = true
    config.angular_templates.ignore_prefix  = %w(angular/templates/)
    config.assets.configure do |env|
      env.cache = ThreadSafe::Cache.new
    end
  end
end
