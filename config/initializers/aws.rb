require 'aws-sdk-v1'

aws_data = Rails.application.secrets[:aws]
Aws.config.update({region: 'us-east-1',credentials: Aws::Credentials.new(aws_data['S3_ACCESS_KEY'], aws_data['S3_SECRET'])})
s3 = Aws::S3::Resource.new
S3_BUCKET = s3.bucket(aws_data['S3_BUCKET_NAME'])