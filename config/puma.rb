workers 2
threads_count = Integer(ENV['MAX_THREADS'] || 5)
threads 1, threads_count

preload_app!

rackup      DefaultRackup
port        ENV['PORT']     || 3000
rails_env = ENV['RAILS_ENV'] || "production"
environment rails_env
#environment ENV['RACK_ENV'] || 'developmentnd "unix://#{shared_dir}/sockets/puma.sock"cuu'


app_dir = File.expand_path("../..", __FILE__)
if rails_env == 'development'
  shared_dir = app_dir
else
  shared_dir = "#{app_dir}/../../shared"
end
#bind "unix://#{shared_dir}/sockets/puma.sock"

stdout_redirect "#{shared_dir}/log/puma.stdout.log", "#{shared_dir}/log/puma.stderr.log", true

# Set master PID and state locations
pidfile "#{shared_dir}/pids/puma.pid"
state_path "#{shared_dir}/pids/puma.state"
activate_control_app

on_worker_boot do
  require "active_record"
  ActiveRecord::Base.connection.disconnect! rescue ActiveRecord::ConnectionNotEstablished
  ActiveRecord::Base.establish_connection(YAML.load_file("#{shared_dir}/config/database.yml")[rails_env])
end

#on_worker_boot do
  # Worker specific setup for Rails 4.1+
  # See: https://devcenter.heroku.com/articles/deploying-rails-applications-with-the-puma-web-server#on-worker-boot
#  ActiveRecord::Base.establish_connection
#end
