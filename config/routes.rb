Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  
  root "tabs#index"
  get 'tabs/:anchor_name' => 'tabs#show'
  resources :tabs, only: [:index, :show]
  resources :assessments, only: [:index, :show] do
    post :save
  end
  get 'search_users' => "users#search_user"
  get 'user/confirm' => "users#confirm"
  get 'search_all_users' => "users#search_all_users"

  devise_for :users, :controllers => 
    {
     :registrations => 'devise_user/registrations',
     sessions: "devise_user/sessions"
    }
  devise_scope :user do
    get "users/:id/add_image" => "devise_user/registrations#add_image"
    patch "users/:id/save_image" => "devise_user/registrations#save_image", as: "upload_profile_pic"
    get 'update_states' => "devise_user/registrations#update_states"
    get 'update_bases' => "devise_user/registrations#update_bases"
  end

  resources :users do
    member do
      get :add_contacts
      post :save_contacts
      get :add_community
      post :save_community
    end
    get :contacts, on: :collection
    post :report_bug, on: :collection
  end
  resources :content_blocks do
    resources :content_block_posts, only: [:index, :create, :new] do
      get :new_reply
      post :save_reply
      get :replies
    end
    member do
      get :index_content_block, controller: :content_block_posts
      get :contacts_and_communities, controller: :content_block_posts
      post :email, controller: :content_block_posts
    end
    post :webhook_notify, controller: :content_block_posts, on: :collection
    post :modify_video_params, controller: :content_block_posts, on: :collection
    get :check_video_upload_status, controller: :content_block_posts, on: :collection
  end
  resources :content_block_posts do
    resources :replies
  end
  resources :replies
  
  namespace :admin do
    resources :tabs do
      resources :sections do
        resources :content_blocks do
          get :get_form_partial, on: :collection 
          get :get_edit_form_partial, on: :member
          get :get_edit_form_partial, on: :collection
        end
      end
    end
    resources :assessments do
      resources :score_ranges do
        post :save_capability, on: :member
      end
      resources :questions do
        post :save_stops, on: :member
      end
    end
    resources :lpds, :path => "lpds" do
      resources :concepts, :path => "concepts"
    end 
    resources :leader_professional_development, :path => "lpds" do
      resources :concepts, :path => "concepts"
    end
  end

  resources :tags
  
  
  get 'onboarding_community_show' => "pages#onboarding_community_show"
  get 'onboarding_community_addcontact' => "pages#onboarding_community_addcontact"
  get 'lpd/:id' => "pages#lpd"
  get 'onboarding_community' => "user_communities#show"
  post "user/community/save" => "user_communities#save_community"
  post "user/allcommunity/save" => "user_communities#save_communities"
  get "user/community/:id/delete" => "user_communities#delete_community"
  get "user/community/:community_id/contact/:contact_id/delete" => "user_communities#delete_community_user"
  get "user/community/:community_id/contact/:contact_id/save" => "user_communities#add_community_user"

  #Template 1
  get 'templates/1-assignment_demands_assessment_1' => "templates#1-assignment_demands_assessment_1"
  get 'templates/1-assignment_demands_assessment_2' => "templates#1-assignment_demands_assessment_2"
  get 'templates/1-assignment_demands_assessment_3' => "templates#1-assignment_demands_assessment_3"
  get 'templates/1-assignment_demands_assessment_4' => "templates#1-assignment_demands_assessment_4"
  get 'templates/1-assignment_demands_assessment_5' => "templates#1-assignment_demands_assessment_5"
  get 'templates/1-assignment_demands_assessment_summary' => "templates#1-assignment_demands_assessment_summary"

  #Template 2
  get 'templates/2-be_a_mentor_1' => "templates#2-be_a_mentor_1"
  get 'templates/2-be_a_mentor_2' => "templates#2-be_a_mentor_2"
  get 'templates/2-be_a_mentor_3' => "templates#2-be_a_mentor_3"
  get 'templates/2-be_a_mentor_4' => "templates#2-be_a_mentor_4"
  get 'templates/2-be_a_mentor_5' => "templates#2-be_a_mentor_5"
  get 'templates/2-be_a_mentor_6' => "templates#2-be_a_mentor_6"
  get 'templates/2-be_a_mentor_7' => "templates#2-be_a_mentor_7"
  get 'templates/2-be_a_mentor_summary' => "templates#2-be_a_mentor_summary"
  
  #Template 3 
  get 'templates/3-command_unit_leader_development_scorecard_1' => 'templates#3-command_unit_leader_development_scorecard_1'
  get 'templates/3-command_unit_leader_development_scorecard_2' => 'templates#3-command_unit_leader_development_scorecard_2'
  get 'templates/3-command_unit_leader_development_scorecard_3' => 'templates#3-command_unit_leader_development_scorecard_3'
  get 'templates/3-command_unit_leader_development_scorecard_4' => 'templates#3-command_unit_leader_development_scorecard_4'
  get 'templates/3-command_unit_leader_development_scorecard_5' => 'templates#3-command_unit_leader_development_scorecard_5'
  get 'templates/3-command_unit_leader_development_scorecard_summary' => 'templates#3-command_unit_leader_development_scorecard_summary'

  #Template 4
  get 'templates/4-daily_leader_observation_planner_1' => 'templates#4-daily_leader_observation_planner_1'
  get 'templates/4-daily_leader_observation_planner_2' => 'templates#4-daily_leader_observation_planner_2'
  get 'templates/4-daily_leader_observation_planner_3' => 'templates#4-daily_leader_observation_planner_3'
  get 'templates/4-daily_leader_observation_planner_summary' => 'templates#4-daily_leader_observation_planner_summary'

  #Template 5
  get 'templates/5-getting_to_know_tool_1' => 'templates#5-getting_to_know_tool_1'
  get 'templates/5-getting_to_know_tool_2' => 'templates#5-getting_to_know_tool_2'
  get 'templates/5-getting_to_know_tool_3' => 'templates#5-getting_to_know_tool_3'
  get 'templates/5-getting_to_know_tool_4' => 'templates#5-getting_to_know_tool_4'
  get 'templates/5-getting_to_know_tool_5' => 'templates#5-getting_to_know_tool_5'
  get 'templates/5-getting_to_know_tool_6' => 'templates#5-getting_to_know_tool_6'
  get 'templates/5-getting_to_know_tool_7' => 'templates#5-getting_to_know_tool_7'
  get 'templates/5-getting_to_know_tool_summary' => 'templates#5-getting_to_know_tool_summary'
  
  #Template 6
  get 'templates/6-individual_plans_for_advancement_1' => 'templates#6-individual_plans_for_advancement_1'
  get 'templates/6-individual_plans_for_advancement_2' => 'templates#6-individual_plans_for_advancement_2'
  get 'templates/6-individual_plans_for_advancement_3' => 'templates#6-individual_plans_for_advancement_3'  
  get 'templates/6-individual_plans_for_advancement_4' => 'templates#6-individual_plans_for_advancement_4' 
  get 'templates/6-individual_plans_for_advancement_5' => 'templates#6-individual_plans_for_advancement_5'  
  get 'templates/6-individual_plans_for_advancement_6' => 'templates#6-individual_plans_for_advancement_6'
  get 'templates/6-individual_plans_for_advancement_summary' => 'templates#6-individual_plans_for_advancement_summary'  

  #Template 7
  get 'templates/7-leadership_selection_guidance_1' => 'templates#7-leadership_selection_guidance_1'
  get 'templates/7-leadership_selection_guidance_2' => 'templates#7-leadership_selection_guidance_2'
  get 'templates/7-leadership_selection_guidance_3' => 'templates#7-leadership_selection_guidance_3'  
  get 'templates/7-leadership_selection_guidance_4' => 'templates#7-leadership_selection_guidance_4' 
  get 'templates/7-leadership_selection_guidance_5' => 'templates#7-leadership_selection_guidance_5'  
  get 'templates/7-leadership_selection_guidance_6' => 'templates#7-leadership_selection_guidance_6'
  get 'templates/7-leadership_selection_guidance_7' => 'templates#7-leadership_selection_guidance_7'  
  get 'templates/7-leadership_selection_guidance_8' => 'templates#7-leadership_selection_guidance_8'
  get 'templates/7-leadership_selection_guidance_summary' => 'templates#7-leadership_selection_guidance_summary'  

  #Template 8
  get 'templates/8-learning_principles_questionaire_a_1' => 'templates#8-learning_principles_questionaire_a_1'
  get 'templates/8-learning_principles_questionaire_a_2' => 'templates#8-learning_principles_questionaire_a_2'
  get 'templates/8-learning_principles_questionaire_a_3' => 'templates#8-learning_principles_questionaire_a_3'
  get 'templates/8-learning_principles_questionaire_a_4' => 'templates#8-learning_principles_questionaire_a_4'
  get 'templates/8-learning_principles_questionaire_a_5' => 'templates#8-learning_principles_questionaire_a_5'
  get 'templates/8-learning_principles_questionaire_a_6' => 'templates#8-learning_principles_questionaire_a_6'
  get 'templates/8-learning_principles_questionaire_a_7' => 'templates#8-learning_principles_questionaire_a_7'
  get 'templates/8-learning_principles_questionaire_a_summary' => 'templates#8-learning_principles_questionaire_a_summary'

  #Template 9
  get 'templates/9-learning_principles_questionaire_b_1' => 'templates#9-learning_principles_questionaire_b_1'
  get 'templates/9-learning_principles_questionaire_b_2' => 'templates#9-learning_principles_questionaire_b_2'
  get 'templates/9-learning_principles_questionaire_b_3' => 'templates#9-learning_principles_questionaire_b_3'
  get 'templates/9-learning_principles_questionaire_b_summary' => 'templates#9-learning_principles_questionaire_b_summary'
  
  #Template 10
  get 'templates/10-training_and_professional_development_1' => 'templates#10-training_and_professional_development_1'
  get 'templates/10-training_and_professional_development_2' => 'templates#10-training_and_professional_development_2'
  get 'templates/10-training_and_professional_development_3' => 'templates#10-training_and_professional_development_3'
  get 'templates/10-training_and_professional_development_4' => 'templates#10-training_and_professional_development_4'
  get 'templates/10-training_and_professional_development_summary' => 'templates#10-training_and_professional_development_summary'
  
  #Template 11
  get 'templates/11-soar_observation_source_1' => 'templates#11-soar_observation_source_1'
  get 'templates/11-soar_observation_source_2' => 'templates#11-soar_observation_source_2'
  get 'templates/11-soar_observation_source_3' => 'templates#11-soar_observation_source_3'
  get 'templates/11-soar_observation_source_4' => 'templates#11-soar_observation_source_4'
  get 'templates/11-soar_observation_source_summary' => 'templates#11-soar_observation_source_summary'  

  #Template 12
  get 'templates/12-study_tool_1' => 'templates#12-study_tool_1'
  get 'templates/12-study_tool_2' => 'templates#12-study_tool_2'
  get 'templates/12-study_tool_3' => 'templates#12-study_tool_3'
  get 'templates/12-study_tool_4' => 'templates#12-study_tool_4'
  get 'templates/12-study_tool_5' => 'templates#12-study_tool_5'
  get 'templates/12-study_tool_summary' => 'templates#12-study_tool_summary'

  #Template 13
  get 'templates/13-tiger_team_implementation_tool_1' => 'templates#13-tiger_team_implementation_tool_1'
  get 'templates/13-tiger_team_implementation_tool_2' => 'templates#13-tiger_team_implementation_tool_2'
  get 'templates/13-tiger_team_implementation_tool_3' => 'templates#13-tiger_team_implementation_tool_3'
  get 'templates/13-tiger_team_implementation_tool_4' => 'templates#13-tiger_team_implementation_tool_4'
  get 'templates/13-tiger_team_implementation_tool_5' => 'templates#13-tiger_team_implementation_tool_5'
  get 'templates/13-tiger_team_implementation_tool_6' => 'templates#13-tiger_team_implementation_tool_6'
  get 'templates/13-tiger_team_implementation_tool_summary' => 'templates#13-tiger_team_implementation_tool_summary'                                     

  #Template 14
  get 'templates/14-unit_leader_development_roles_and_responsibilities_1' => 'templates#14-unit_leader_development_roles_and_responsibilities_1'
  get 'templates/14-unit_leader_development_roles_and_responsibilities_2' => 'templates#14-unit_leader_development_roles_and_responsibilities_2'
  get 'templates/14-unit_leader_development_roles_and_responsibilities_3' => 'templates#14-unit_leader_development_roles_and_responsibilities_3'
  get 'templates/14-unit_leader_development_roles_and_responsibilities_summary' => 'templates#14-unit_leader_development_roles_and_responsibilities_summary'
 
  
  get "templates/:type/(:user_template_id)" => "templates#show", as: :user_template
  delete "templates/:id" => "templates#destroy", as: :delete_user_template
  post "templates/save" => "templates#save"
  post "templates/send" => "templates#send_template"
  ActiveAdmin.routes(self)
  
end
